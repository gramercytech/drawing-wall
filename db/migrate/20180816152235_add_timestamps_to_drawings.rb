class AddTimestampsToDrawings < ActiveRecord::Migration[5.1]
  def change
    add_timestamps :drawings, default: Time.now
    # SHOULD BE
    # add_timestamps(:drawings, null: false)
  end
end
