class AddTestToDrawings < ActiveRecord::Migration[5.1]
  def change
    add_column :drawings, :test, :boolean, default: false
  end
end
