class AddSharePageStylesToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :show_drawing_styles, :text, :limit => 999999999
  end
end
