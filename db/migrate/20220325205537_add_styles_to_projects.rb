class AddStylesToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :styles, :text, :limit => 999999999
  end
end
