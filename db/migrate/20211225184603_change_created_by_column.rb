class ChangeCreatedByColumn < ActiveRecord::Migration[5.1]
  def change
    rename_column :projects, :created_by_id, :created_by
  end
end
