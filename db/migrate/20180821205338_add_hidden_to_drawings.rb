class AddHiddenToDrawings < ActiveRecord::Migration[5.1]
  def change
    add_column :drawings, :hidden, :boolean, default: false
  end
end
