class AddIpadStylesToProjects < ActiveRecord::Migration[5.1]
  def change
    rename_column :projects, :styles, :wall_styles
    add_column :projects, :tablet_styles, :text, :limit => 999999999
  end
end
