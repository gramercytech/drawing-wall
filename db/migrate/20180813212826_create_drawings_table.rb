class CreateDrawingsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :drawings do |t|
      t.string :name
      t.string :email
      t.string :country
      t.string :city
      t.boolean :accepts_terms
    end
  end
end
