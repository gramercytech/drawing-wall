class AddProjectTables < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.references :created_by, index: true, foreign_key: { to_table: :admins }
      t.json :config
      t.timestamps null: false
    end

    create_join_table :admins, :projects, table_name: :admin_projects

    create_table :project_invites do |t|
      t.references :admin, foreign_key: true
      t.references :project, foreign_key: true
      t.timestamps null: false
    end
  end
end
