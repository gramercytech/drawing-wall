class AddImageToDrawings < ActiveRecord::Migration[5.1]
  def change
    add_column :drawings, :image, :string
  end
end
