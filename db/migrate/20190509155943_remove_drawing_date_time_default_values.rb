class RemoveDrawingDateTimeDefaultValues < ActiveRecord::Migration[5.1]
  def change
    change_column_default(:drawings, :created_at, nil)
    change_column_default(:drawings, :updated_at, nil)
  end
end
