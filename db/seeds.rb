# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# i = 1

# while i < 6 do
#   puts i
#   Drawing.create(
#     image: File.open(Rails.root + "lib/assets/demo#{i}.png"),
#   )
#   i += 1
# end

# Because we use Time.now for filename, these ran so fast they all had the same timestamp and overwrote each other
Drawing.create( image: File.open(Rails.root + "lib/assets/demo1.png") )
sleep 1
Drawing.create( image: File.open(Rails.root + "lib/assets/demo2.png") )
sleep 1
Drawing.create( image: File.open(Rails.root + "lib/assets/demo3.png") )
sleep 1
Drawing.create( image: File.open(Rails.root + "lib/assets/demo4.png") )
sleep 1
Drawing.create( image: File.open(Rails.root + "lib/assets/demo5.png") )