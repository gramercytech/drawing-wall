# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20230911201524) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_projects", force: :cascade do |t|
    t.bigint "admin_id", null: false
    t.bigint "project_id", null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "drawings", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "country"
    t.string "city"
    t.boolean "accepts_terms"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "hidden", default: false
    t.boolean "test", default: false
    t.bigint "project_id"
    t.index ["project_id"], name: "index_drawings_on_project_id"
  end

  create_table "project_invites", force: :cascade do |t|
    t.bigint "admin_id"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_project_invites_on_admin_id"
    t.index ["project_id"], name: "index_project_invites_on_project_id"
  end

  create_table "projects", force: :cascade do |t|
    t.bigint "created_by"
    t.json "config"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.text "wall_styles"
    t.text "tablet_styles"
    t.text "show_drawing_styles"
    t.index ["created_by"], name: "index_projects_on_created_by"
  end

  add_foreign_key "drawings", "projects"
  add_foreign_key "project_invites", "admins"
  add_foreign_key "project_invites", "projects"
  add_foreign_key "projects", "admins", column: "created_by"
end
