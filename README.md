versions of stuff (as of 09/12/32)
-----------------------------------

- npm:  v6.14.18
- node: v14.21.3
- ruby: v2.7.0


commands
------------
`git clone git@bitbucket.org:gramercytech/drawing-wall.git`

`cd drawing-wall`

update config/database.yml with your local credentials

`bundle install`

`yarn` (using node@14)

`cp ./application.yml.example ./config/application.yml`

update config/application.yml with real credentials

`rake db:create`

`rake db:migrate`

`rails s` to start server

`rails c` in another tab for console

create an admin with console

```
admin = Admin.new
admin.email = "your@email.here"
admin.password = "testing123"
admin.save
```

if you get an error on Mac with in rails console: `objc[58329]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called.`
try `spring stop` before you start up the console


log in at http://localhost:3000

after creating a project you can view the wall and the ipad app from the dashboard

from the project admin screen you can edit settings, export drawings and manage collaborators

use the [gtkiosk](https://tinyurl.com/gtkiosk) app to run the ipad app

if the wall is running off a machine other than the server, you'll need to update `config.action_cable.url` in config/environments/development.rb to point to the IP address of the server machine rather than localhost

to redeploy run `bundle exec cap production deploy` user will need to have their ssh key on the server

to restart the app, you can ssh into server and run `~/.rvm/bin/rvm default do bundle exec puma -C /home/ubuntu/drawing-wall/shared/puma.rb --daemon`


example config JSON
---------------------------------
```
{
	"grid_section_count": 6,
	"live_placement_x": 3,
	"live_placement_y": 4,
	"live_columns": 2,
	"live_rows": 2,
	"grid_section_columns": 6,
	"grid_section_rows": 9,
	"idle_image": "https://drawingwall-ef.s3.amazonaws.com/static/6xidle.png",
	"bg_image": "https://drawingwall-ef.s3.amazonaws.com/static/6xbackground.jpeg",
	"ipad": {
		"colors": ["#ffffff", "#4a3c31", "#a2ad00", "#b2541a", "#60351d", "#eaab00", "#589199", "#a79e70", "#983222", "#827c34", "#622567", "#ffff00"],
		"start_screen": {
			"image": "https://drawingwall-ef.s3.amazonaws.com/static/gt-start.jpeg",
			"button_y": 504,
			"button_width": 426,
			"button_height": 210,
			"button_opacity": 0
		},
		"thankyou_screen": {
			"image": "https://drawingwall-ef.s3.amazonaws.com/static/gt-thanks.jpeg",
			"button_x": "300px",
			"button_y": "504px",
			"button_width": "600px",
			"button_height": "100px",
			"button_opacity": "0",
			"qr_code": true,
			"qr_code_top": "130px",
			"qr_code_width": "300px",
			"qr_bg_color": "#FFFFFF",
			"qr_fg_color": "#000000"
		},
		"signup_screen": false,
		"canvas_ratio": 0.75
	}
}
```