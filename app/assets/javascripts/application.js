// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
//= require_tree ./channels

window.refresh = function () {
  window.location.reload()
}

window.fadein = function (element) {
  setTimeout(function () {
    element.classList.remove('hidden')
  }, 100)
}

window.fadeout = function (element) {
  element.classList.add('hidden')
}

window.unglow = function (element) {
  setTimeout(function () {
    element.classList.remove('glowing')
  }, 100)
}

window.getUrlParam = function (parameter, defaultValue) {
  var urlparameter = defaultValue
  if (window.location.href.indexOf(parameter) > -1) urlparameter = getUrlVars()[parameter]
  return urlparameter
}

function getUrlVars () {
  var vars = {}
  window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
    vars[key] = value
  })
  return vars
}
