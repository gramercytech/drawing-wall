module Api
  class CollaboratorsController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_project
    before_action :set_collaboration, only: :destroy
    before_action :check_admin_has_access, only: :index
    before_action :check_admin_can_remove, only: :destroy

    def index
      render json: @project.collaborations.to_json(include: :admin), status: :ok
    end

    def destroy
      @collaboration.destroy
      render json: nil, status: :no_content
    end

    private

    def set_project
      @project = Project.find_by(id: params[:project_id])
      render json: {project: ['not_found']}, status: :not_found unless @project
    end

    def set_collaboration
      @collaboration = AdminProject.find_by(id: params[:id])
      render json: {invite: ['not found']}, status: :not_found unless @collaboration
    end

    def check_admin_has_access
      render json: {
        admin: ['is neither creator nor collaborator on project']
      }, status: :forbidden unless current_admin.can_view(@project)
    end

    def check_admin_can_remove
      unless @project.created_by == current_admin || @collaboration.admin == current_admin
        render json: {
          you: ['are neither the project creator nor the collaborator you are trying to remove']
        }, status: :forbidden
      end
    end

  end
end
