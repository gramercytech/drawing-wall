module Api
  class AdminsController < ApplicationController
    include ApplicationHelper
    
    before_action :authenticate_admin!
    before_action :check_admin_access, only: %i[ index show update ]

    def index
      admins = Admin.all
      render json: { admins: admins }, status: :ok
    end

    def show
      render json: { admin: @admin }, status: :ok
    end

    def create
      # Rails.logger.info 'test????'
      # Rails.logger.info admin_params

      @admin = Admin.new(admin_params)
      # @admin = Admin.new()
      # @admin.email = 'test@test.com'
      # @admin.password = 'testing123'
      if @admin.save
        render json: { admin: @admin }, status: :created
      else
        Rails.logger.debug @admin.errors.full_messages.to_sentence
        render json: @admin.errors.messages, status: :unprocessable_entity
      end
    end

    def update
      if @admin.update(admin_params)
        render json: { admin: @admin }, status: :accepted
      else
        render json: @project.errors.messages, status: :unprocessable_entity
      end
    end

    private

    def set_project
      @project = Project.find_by(id: params[:id])
      render json: { project: 'not found' }, status: :not_found unless @project
    end

    def check_admin_access
      render json: { access: 'is forbidden' }, status: :forbidden unless is_gramercy_email(current_admin.email)
    end

    def admin_params
      Rails.logger.info params
      params.require(:admin).permit(:email, :password, :password_confirmation)
    end
  end
end
