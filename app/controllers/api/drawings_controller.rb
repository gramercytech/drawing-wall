module Api
  class DrawingsController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_project
    before_action :set_drawing, only: :update
    before_action :check_admin_access

    def index
      render json: { drawings: @project.drawings }, status: :ok
    end

    def update
      if @drawing && @drawing.update(update_params)
        ActionCable.server.broadcast "project_#{@project.id}", {
          type: 'admin-update', drawing: @drawing
        }
        render json: @drawing, status: :accepted
      else
        render json: @drawing.errors.messages, status: :bad_request
      end
    end

    private

    def set_drawing
      @drawing = Drawing.find_by(id: params[:id])
      render json: nil, status: :not_found unless @drawing
    end

    def set_project
      @project = Project.find_by(id: params[:project_id])
      render json: nil, status: :not_found unless @project
    end

    def check_admin_access
      render json: {}, status: :forbidden unless current_admin.can_view(@project)
    end

    def update_params
      params.require(:drawing).permit(:hidden, :test)
    end
  end
end
