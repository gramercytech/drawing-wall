module Api
  class ProjectInvitesController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_project
    before_action :check_admin_has_access, only: :index
    before_action :check_admin_is_project_creator, only: [:create, :destroy]
    before_action :set_invite, only: :destroy

    def index
      render json: @project.invites.to_json(
        include: :admin
      ), status: :ok
    end

    def create
      admin = Admin.find_by(email: invite_params[:email])
      if admin
        invite = ProjectInvite.new(admin: admin, project: @project)
        if invite.save
          render json: invite, status: :created
        else
          render json: invite.errors.messages, status: :unprocessable_entity
        end
      else
        render json: {email: ['not found']}, status: :not_found unless admin
      end
    end

    def destroy
      @invite.destroy
      render json: nil, status: :no_content
    end

    private

    def set_project
      @project = Project.find_by(id: params[:project_id])
      render json: {project: ['not_found']}, status: :not_found unless @project
    end

    def set_invite
      @invite = ProjectInvite.find_by(id: params[:id])
      render json: {invite: ['not found']}, status: :not_found unless @invite
    end

    def check_admin_has_access
      render json: {
        admin: ['is neither creator nor collaborator on project']
      }, status: :forbidden unless current_admin.can_view(@project)
    end

    def check_admin_is_project_creator
      render json: {
        admin: ['is not creator of project']
      }, status: :forbidden unless @project.created_by == current_admin
    end

    def invite_params
      params.require(:invite).permit(:email)
    end

  end
end
