module Api
  class AdminInvitesController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_invite, only: [:update, :destroy]
    before_action :check_admin_access, only: %i[ update destroy ]

    def index
      # this is the list of invites belonging to an admin
      # invites belonging to a project are at api/project/project_invites_controller
      invites = current_admin.invites.as_json(include: :project)
      render json: { status: :ok, invites: invites, message: 'Success' }
    end

    def update
      @invite.accept
      render json: { status: :ok, message: 'Success' }
    end

    def destroy
      @invite.reject
      render json: { status: :no_content, message: 'Success' }
    end

    private

    def set_invite
      @invite = ProjectInvite.find_by(id: params[:id])
      render json: nil, status: :not_found unless @invite
    end

    def check_admin_access
      render json: nil, status: :forbidden unless current_admin.can_view(@invite)
    end
  end
end
