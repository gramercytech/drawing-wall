module Api
  class ProjectsController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_project, only: %i[ show update destroy ]
    before_action :check_admin_access, only: %i[ show update destroy ]

    def index
      projects = current_admin.projects_and_collaborations
      render json: { projects: projects }, status: :ok
    end

    def show
      render json: { project: @project }, status: :ok
    end

    def create
      project = Project.new(project_params.merge(created_by: current_admin))
      if project.save
        render json: { project: project }, status: :created
      else
        render json: project.errors.messages, status: :unprocessable_entity
      end
    end

    def update
      if @project.update(project_params)
        render json: { project: @project }, status: :accepted
      else
        render json: @project.errors.messages, status: :unprocessable_entity
      end
    end

    def destroy
      if @project.created_by == current_admin
        @project.destroy
        render json: @project, status: :no_content
      else
        render json: {}, status: :forbidden
      end
    end

    private

    def set_project
      @project = Project.find_by(id: params[:id])
      render json: { project: 'not found' }, status: :not_found unless @project
    end

    def check_admin_access
      render json: { access: 'is forbidden' }, status: :forbidden unless current_admin.can_view(@project)
    end

    def project_params
      params.require(:project).permit(:name, :config, :wall_styles, :tablet_styles, :show_drawing_styles)
    end
  end
end
