class AdminsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_project, except: %i[ index ]

  def show
    @drawings = drawings
    @config = @project.config || Rails.application.config_for(:drawings).to_json
    @config['project_id'] = @project.id
    @config['styles'] = @project.wall_styles
    render :layout => 'experience'
  end

  def edit
    @drawings = @project.drawings
    @config = @project.config || Rails.application.config_for(:drawings).to_json
    @invites = @project.invites
    @collaborations = @project.collaborations
    @new_invite = ProjectInvite.new(project: @project)
  end

  def update
    if @project.update(project_params)
      redirect_to edit_project_url(@project), notice: "Project was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @project.destroy
    redirect_to projects_url, notice: "Project was successfully destroyed."
  end

  private

  def set_project
   @project = Project.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:name, :config)
  end

  def drawings
    params[:test].presence ?
      @project.drawings.where(hidden: false).order(created_at: :desc) : 
      @project.drawings.where(hidden: false, test: false).order(created_at: :desc)
  end
end
