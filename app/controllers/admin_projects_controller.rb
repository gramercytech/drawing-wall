class AdminProjectsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_collaboration, only: [:destroy]

  def destroy
    project = @collaboration.project
    @collaboration.destroy
    redirect_to edit_project_path(project)
  end

  private

  def set_collaboration
   @collaboration = AdminProject.find(params[:id])
  end
end

