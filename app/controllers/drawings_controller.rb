class DrawingsController < ApplicationController
  layout 'experience'
  before_action :authenticate_admin!, except: :show
  before_action :set_project

  def new
    @ipadParam = 'ipad' + params[:ipad] || 'ipad0'
    @config = @project.config['ipad'] || Rails.application.config_for(:drawings).to_json['ipad']
    @config['project_id'] = @project.id
    @styles = @project.tablet_styles
  end

  def show
    @drawing = Drawing.find(params['id'])
    @styles = @project.show_drawing_styles
  end

  # TODO move to api
  def create
    if drawing = Drawing.create(drawing_params.merge(project: @project))
      ipad = params[:ipad] ? params[:ipad] : 0
      ActionCable.server.broadcast "project_#{@project.id}", { type: 'complete', drawing: drawing, ipad: ipad }
      render json: { status: 201, message: 'Success', data: drawing }
    else
      render json: { status: 400, message: 'Error' }
    end
  end

  # TODO move to api
  def update
    drawing = Drawing.find(params[:id])
    if drawing && drawing.update(update_params)
      ActionCable.server.broadcast "project_#{@project.id}", { type: 'admin-update', drawing: drawing }
      render json: { status: 201, message: 'Success', data: drawing }
    else
      render json: { status: 400, message: 'Error' }
    end
  end

  # TODO move to api
  def live_update
    type = params[:reset] ? 'reset' : 'update'
    ipad = params[:ipad] ? params[:ipad] : 0
    ActionCable.server.broadcast "project_#{@project.id}", { type: type, drawing: params[:drawing], ipad: ipad }
    render json: { status: 200, message: 'Success' }
  end

  private

  def set_project
    @project = Project.find(params[:project_id])
  end

  def drawing_params
    params.require(:drawing).permit(:image)
  end

  def update_params
    params.require(:drawing).permit(:hidden, :id, :name, :email, :accepts_terms)
  end
end
