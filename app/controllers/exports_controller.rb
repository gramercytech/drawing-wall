class ExportsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_project

  include Zipline

  def create
    @drawings = @project.drawings
    files = @drawings.map { |d| [d.image, d.image_identifier] }
    csv = File.new("#{Rails.root.to_s}/tmp/drawings.txt", 'w')
    csv.write(@drawings.to_csv)
    csv.close
    files << [csv, 'csv.csv']
    send_zip(files, 'drawings.zip')
  end

  private

  def send_zip(files, zipname)
    zip_generator = ZipGenerator.new(files)
    def zip_generator.is_io?(file)
      false
    end
    headers['Content-Disposition'] = "attachment; filename=\"#{zipname.gsub '"', '\"'}\""
    headers['Content-Type'] = Mime::Type.lookup_by_extension('zip').to_s
    response.sending_file = true
    response.cache_control[:public] ||= false
    self.response_body = zip_generator
    self.response.headers['Last-Modified'] = Time.now.httpdate
  end

  def set_project
    @project = Project.find(params[:project_id])
  end
end
