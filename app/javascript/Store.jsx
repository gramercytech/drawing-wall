import React, { useReducer, useEffect } from 'react'
export const Context = React.createContext()

const initialState = {
  currentAdmin: null,
  admins: [],
  projects: [],
  currentProject: null,
  drawings: [],
  collaborators: [],
  projectInvites: [],
  invites: []
}

export const setAdmin = (admin) => ({ type: 'SET_ADMIN', admin })
export const setProjects = (projects) => ({ type: 'SET_PROJECTS', projects })
export const setAdmins = (admins) => ({ type: 'SET_ADMINS', admins })
export const setCurrentProject = (project) => ({ type: 'SET_PROJECT', project })
export const setDrawings = (drawings) => ({ type: 'SET_DRAWINGS', drawings })
export const setCollaborators = (collabs) => ({ type: 'SET_COLLABORATORS', collabs })
export const setProjectInvites = (invites) => ({
  type: 'SET_PROJECT_INVITES', invites
})
export const setInvites = (invites) => ({ type: 'SET_INVITES', invites })

export const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_ADMIN': return { ...state, currentAdmin: action.admin }
    case 'SET_PROJECTS': return { ...state, projects: action.projects }
    case 'SET_ADMINS': return { ...state, admins: action.admins }
    case 'SET_PROJECT': return { ...state, currentProject: action.project }
    case 'SET_DRAWINGS': return { ...state, drawings: action.drawings }
    case 'SET_COLLABORATORS': return { ...state, collaborators: action.collabs }
    case 'SET_PROJECT_INVITES': return { ...state, projectInvites: action.invites }
    case 'SET_INVITES': return { ...state, invites: action.invites }

    default:
      console.warn('UNKNOWN ACTION', action.type)
      return state
  }
}

const Store = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const debug = false

  useEffect(() => {
    if (debug) console.log('%c Store %o', 'color: green; font-weight: bold;', state)
  }, [state])

  return (
    <Context.Provider value={{ state, dispatch }}>{props.children}</Context.Provider>
  )
}

export default Store
