import { Urls } from "./urls";
import { get, put, httpDelete } from "./Http";

const apiUrl = Urls().api;
const adminInvitesPath = `${apiUrl}/admin_invites`;

export const getAdminInvites = () => get(`${adminInvitesPath}`);
export const acceptAdminInvite = (id) => put(`${adminInvitesPath}/${id}`);
export const rejectAdminInvite = (id) =>
  httpDelete(`${adminInvitesPath}/${id}`);
