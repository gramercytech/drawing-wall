import { Urls } from "./urls";
import { get, httpDelete } from "./Http";

const apiUrl = Urls().api;
const projectsPath = `${apiUrl}/projects`;

export const getCollaborators = (projectId) =>
  get(`${projectsPath}/${projectId}/collaborators`);

export const removeCollaboration = (collab) =>
  httpDelete(`${projectsPath}/${collab.project_id}/collaborators/${collab.id}`);
