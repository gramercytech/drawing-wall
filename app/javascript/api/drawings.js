import { baseProjectsPath, projectsPath } from "./projects";
import { get, put } from "./Http";

export const getDrawings = (projectId) =>
  get(`${projectsPath}/${projectId}/drawings`);

export const updateDrawing = (drawing) =>
  put(`${projectsPath}/${drawing.project_id}/drawings/${drawing.id}`, {
    drawing,
  });

export const exportDrawings = (drawing) =>
  get(`${projectsPath}/${projectId}/exports`);

export const liveUpdateDrawing = (projectId, drawing, ipad) =>
  put(`${baseProjectsPath}/${projectId}/drawings/live_update`, {
    drawing,
    ipad,
  });

export const liveUpdateReset = (projectId, ipad) =>
  put(`${baseProjectsPath}/${projectId}/drawings/live_update`, {
    ipad,
    reset: true,
  });
