import { Urls } from "./urls";
import { get, post, put, httpDelete } from "./Http";

const apiUrl = Urls().api;
const baseUrl = Urls().base;

export const adminsPath = `${apiUrl}/admins`;
export const baseProjectsPath = `${baseUrl}/admins`;

export const getAdmins = () => get(`${adminsPath}`);
export const getAdmin = (id) => get(`${adminsPath}/${id}`);
export const createAdmin = (admin) => post(`${adminsPath}`, admin);
export const updateAdmin = (admin) =>
  put(`${adminsPath}/${admin.id}`, admin);
export const deleteAdmin = (admin) =>
  httpDelete(`${adminsPath}/${admin.id}`, admin);
