import axios from 'axios'

var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')

export const get = (url) => axios.get(url).then((res) => {
  return res.data
}).catch((err) => {
  throw err.response.data
})

export const post = (url, body) => axios.post(url, body, {
  headers: { 'X-CSRF-Token': token }
}).then((res) => {
  return res.data
}).catch((err) => {
  throw err.response.data
})

export const put = (url, body) => axios.put(url, body, {
  headers: { 'X-CSRF-Token': token }
}).then((res) => {
  return res.data
}).catch((err) => {
  throw err.response.data
})

export const httpDelete = (url) => axios.delete(url, {
  headers: { 'X-CSRF-Token': token }
}).then((res) => {
  return res.data
}).catch((err) => {
  throw err.response.data
})
