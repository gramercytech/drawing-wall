export const Urls = () => {
  const baseUrl = {
    development: 'http://localhost:3000',
    production: 'https://onsite.drawwall.co'
  }[process.env.RAILS_ENV]

  return {
    base: baseUrl,
    api: `${baseUrl}/api`
  }
}
