import { Urls } from "./urls";
import { get, post, put, httpDelete } from "./Http";

const apiUrl = Urls().api;
const baseUrl = Urls().base;

export const projectsPath = `${apiUrl}/projects`;
export const baseProjectsPath = `${baseUrl}/projects`;

export const getProjects = () => get(`${projectsPath}`);
export const getProject = (id) => get(`${projectsPath}/${id}`);
export const createProject = (project) => post(`${projectsPath}`, project);
export const updateProject = (project) =>
  put(`${projectsPath}/${project.id}`, project);
export const deleteProject = (project) =>
  httpDelete(`${projectsPath}/${project.id}`, project);
