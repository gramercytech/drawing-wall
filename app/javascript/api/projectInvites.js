import { Urls } from './urls'
import { get, post, httpDelete } from './Http'

const apiUrl = Urls().api
const projectsPath = `${apiUrl}/projects`

export const getProjectInvites = (projectId) => get(
  `${projectsPath}/${projectId}/invites`
)

export const inviteToProject = (invite) => post(
  `${projectsPath}/${invite.projectId}/invites`, { invite }
)


export const cancelProjectInvite = (invite) => httpDelete(
  `${projectsPath}/${invite.project_id}/invites/${invite.id}`
)


