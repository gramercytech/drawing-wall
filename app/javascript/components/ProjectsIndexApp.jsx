import React from 'react'
import Store from '../Store.jsx'
import { ProjectsIndex } from './dashboard/ProjectsIndex'
import { Toaster } from 'react-hot-toast'

const ProjectsIndexApp = (props) => (
  <Store>
    <ProjectsIndex admin={props.currentAdmin} />
    <Toaster />
  </Store>
)

export default ProjectsIndexApp
