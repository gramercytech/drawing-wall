import React, { useEffect, useRef, useCallback } from "react";
import styled from "styled-components";

const Layout = styled.div`
  margin-bottom: 3em;
`;

const InputWrapper = styled.form`
  margin-top: 2em;
  display: flex;
  flex-direction: row;
  border: 1px solid grey;
  padding: 2px;
  border-radius: 2px;
`;

const UrlInput = styled.input`
  border: none;
  padding: 2px;
  margin: 2px;
  font-size: 1em;
  flex: 1;
  color: #666666;
`;

const CopyButton = styled.div`
  background: #9900cc;
  color: white;
  border-radius: 4px;
  padding: 0.5rem 1rem;
`;

const InfoText = styled.div``;

export const UrlCopyer = ({ url }) => {
  const urlInputRef = useRef(null);

  useEffect(() => {
    console.log("copying");
    copyUrl();
  }, [urlInputRef]);

  const copyUrl = useCallback(() => {
    if (urlInputRef.current) {
      const input = urlInputRef.current;
      input.select();
      document.execCommand("copy");
    }
  }, [urlInputRef]);

  return (
    <Layout>
      <InputWrapper>
        <UrlInput ref={urlInputRef} value={url} readOnly />
        <CopyButton onClick={copyUrl}>Copy</CopyButton>
      </InputWrapper>
      <InfoText>
        Your browser doesn't support click-to-share, please copy the link above
        and share it with your friends!
      </InfoText>
    </Layout>
  );
};
