import React, { useReducer, useEffect } from "react";
export const Context = React.createContext();

const initialState = {
  stage: null,
  penColor: null,
};

const SET_STAGE = "SET_STAGE";
const SET_PEN_COLOR = "SET_PEN_COLOR";

export const setStage = (stage) => ({ type: SET_STAGE, stage });
export const setPenColor = (penColor) => ({ type: SET_PEN_COLOR, penColor });

export const reducer = (state, action) => {
  switch (action.type) {
    case SET_STAGE:
      return { ...state, stage: action.stage };
    case SET_PEN_COLOR:
      return { ...state, penColor: action.penColor };

    default:
      console.warn("UNKNOWN ACTION", action.type);
      return state;
  }
};

const Store = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const debug = false;

  useEffect(() => {
    if (debug)
      console.log("%c Store %o", "color: green; font-weight: bold;", state);
  }, [state, debug]);

  return (
    <Context.Provider value={{ state, dispatch }}>
      {props.children}
    </Context.Provider>
  );
};

export default Store;
