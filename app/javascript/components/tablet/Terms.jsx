import React from "react";
const TermsParagraphs = require("../../TermsParagraphs");

class Terms extends React.Component {
  continueToArt() {
    let accepts = document.getElementById("accepts-terms").checked;
    this.props.setDrawing({ accepts_terms: accepts });
    // this.props.setPage('Thanks', true)
    this.props.setPage("Thanks");
  }

  cancel() {
    this.props.setPage("Art");
  }

  render() {
    let paragraphs = TermsParagraphs.map((p, index) => {
      return <p key={index}>{p}</p>;
    });

    return (
      <div id="terms">
        <h1>Terms and Conditions</h1>
        <div id="terms-container">{paragraphs}</div>
        <div id="term-buttons">
          <div className="term-button">
            <button
              className="button button-outline"
              onClick={this.cancel.bind(this)}
            >
              Back
            </button>
          </div>
          <div className="term-button i-agree">
            <input type="checkbox" id="accepts-terms" name="accepts-terms" />
            <label className="label-inline" htmlFor="accepts-terms">
              I agree
            </label>
          </div>
          <div className="term-button">
            <button className="button" onClick={this.continueToArt.bind(this)}>
              Continue
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Terms;
