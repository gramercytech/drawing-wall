// todo convert me to a functional/hooks
class Art extends React.Component {
  constructor(props) {
    super(props);
    this.handleButtonPress = this.handleButtonPress.bind(this);
    this.handleButtonRelease = this.handleButtonRelease.bind(this);
    this.modalButtons = [
      {
        label: "Back",
        action: this.toggleModal.bind(this),
        class: "button-outline",
      },
      {
        label: "Submit",
        action: this.submit.bind(this),
      },
    ];
    this.exitButtons = [
      {
        label: "Yes",
        action: this.exit.bind(this),
      },
      {
        label: "No",
        action: this.toggleExitModal.bind(this),
        class: "button-outline",
      },
    ];
    this.state = {
      stage: null,
      stageCache: null,
      snapshotList: [],
      undoSize: maxUndo,
      modalOn: false,
      exiting: false,
      backgroundColor: null,
      color: null,
      thickness: 30,
      backgroundSelectorActive: false,
      weightSelectorActive: false,
      iPad: null,
      pen: null,
    };
  }

  addBackgroundColor(color) {
    const canvas = document.getElementById("canvas");
    canvas.style.backgroundColor = color;
    this.setState(
      { backgroundSelectorActive: false, backgroundColor: color },
      () => {
        this.changePen();
        this.updateActiveWallDrawing();
      }
    );
  }

  selectPaletteColor(color) {
    if (this.state.backgroundSelectorActive) this.addBackgroundColor(color);
    else this.changePen(color);
  }

  copyDrawingToStage(stage) {
    for (let i = window.drawing.children.length - 1; i >= 0; i--) {
      const child = window.drawing.children[i];
      stage.addChild(child);
      stage.setChildIndex(child, i - 1);
    }
    stage.update();
  }

  updateActiveWallDrawing() {
    const image = this.convertCanvasToBase64();
    liveUpdateDrawing(
      this.props.settings["project_id"],
      image,
      this.state.ipad
    );
    // this.createSnapshot()
    // if (this.state.stage.numChildren >= this.state.undoSize) this.addSnapshot()
    resetTimer();
  }

  createSnapshot() {
    const image = this.convertCanvasToBase64();
    this.state.snapshotList.push(image);
  }

  addSnapshot() {
    const bm = new window.createjs.Bitmap(this.state.snapshotList.shift());
    bm.type = "snapshot";
    bm.createdAt = new Date();
    const self = this;
    bm.image.onload = () => {
      self.state.stage.removeChildAt(0);
      self.state.stage.addChildAt(bm, 0);
      if (this.state.stage.numChildren > maxUndo) {
        var oldestChild = this.state.stage.children.find((child, index) => {
          if (child.type !== "snapshot") child.arrayPosition = index;
          return child.type !== "snapshot";
        });
      }
      if (oldestChild)
        this.state.stage.removeChildAt(oldestChild.arrayPosition);
      // this.state.stage.update()
    };
  }

  refresh() {
    // window.refresh()
    // this.props.setPage('Start')
    this.exit();
  }

  startTimer() {
    window.idleTimeoutID = window.setTimeout(this.refresh.bind(this), 60000);
  }

  componentWillUnmount() {
    resetTimer();
  }

  componentDidMount() {
    document.addEventListener("keypress", (event) => {
      if (event.key === "a") {
        this.state.stage.children.forEach((child) => {
          if (child.type !== "snapshot") this.state.stage.removeChild(child);
        });
      }
    });

    var stage = new window.createjs.Stage("canvas");
    window.stage = stage;
    var iPad = window.getUrlParam("ipad", "0");
    window.createjs.Touch.enable(stage, true, false);
    this.setState({ stage, iPad }, () => {
      stage.update();
      this.changePen();
    });

    // Start Idle Timer
    this.startTimer();
  }

  changePen(color, thickness) {
    console.log("changePen");
    if (!color) color = this.state.color;
    if (!color) color = "#000000";
    if (!thickness) thickness = this.state.thickness;
    if (this.state.pen) this.state.pen.destroy();
    if (this.state.stage.numChildren > this.state.undoSize) this.addSnapshot();
    const self = this;
    const pen = Pen(
      this.state.stage,
      color,
      thickness,
      () => {
        self.updateActiveWallDrawing();
        this.createSnapshot();
        if (this.state.stage.numChildren >= this.state.undoSize)
          this.addSnapshot();
      },
      () => {
        // console.log('destroying pen')
      }
    );
    console.log("change pen");
    this.setState({
      pen,
      color,
      backgroundSelectorActive: false,
      weightSelectorActive: false,
    });
  }

  cancelChangePenWeight() {
    this.setState({ weightSelectorActive: false });
  }

  changeThickness(thickness) {
    if (!thickness) thickness = this.state.thickness;
    this.setState({ thickness });
    this.changePen(this.state.color, thickness);
  }

  undo() {
    let stage = this.state.stage;
    if (stage.numChildren > 0) {
      let lastChild = stage.children[stage.children.length - 1];
      if (lastChild.type !== "snapshot") {
        stage.removeChild(lastChild);
        const snapshotList = this.state.snapshotList;
        snapshotList.pop();
        this.setState({ snapshotList });
        stage.update();
      }
    }
    const color = this.state.color;
    this.changePen(color);
    this.updateActiveWallDrawing();
  }

  toggleModal() {
    this.changePen(this.state.color);
    const modalOn = !this.state.modalOn;
    this.setState({ modalOn });
  }

  submit() {
    const image = this.convertCanvasToBase64();
    this.props.setDrawing({ image }, true);
    if (this.props.settings.signup_screen) this.props.setPage("NewUser");
    else this.props.setPage("ThankYou");
  }

  exit() {
    const self = this;
    liveUpdateReset(this.props.settings["project_id"], this.state.iPad)
      .then((res) => {
        console.log(res);
        self.props.setPage("Start");
      })
      .catch((e) => {
        console.error(e);
        self.props.setPage("Start");
      });
  }

  toggleExitModal() {
    const exiting = !this.state.exiting;
    this.setState({ exiting });
  }

  toggleThicknessSelect() {
    const weightSelectorActive = !this.state.weightSelectorActive;
    const backgroundSelectorActive = weightSelectorActive
      ? false
      : this.state.backgroundSelectorActive;
    this.setState({ weightSelectorActive, backgroundSelectorActive });
    // document.getElementById('weight-select').classList.toggle('hover')
  }

  toggleBackgroundColorSelect() {
    const backgroundSelectorActive = !this.state.backgroundSelectorActive;
    const weightSelectorActive = backgroundSelectorActive
      ? false
      : this.state.weightSelectorActive;
    this.setState({ backgroundSelectorActive, weightSelectorActive });
  }

  convertCanvasToBase64() {
    let tempBackground = null;
    if (this.state.backgroundColor) {
      tempBackground = BackgroundColor(
        this.state.stage,
        this.state.backgroundColor
      );
    }
    const canvas = document.getElementById("canvas");
    const base64Image = canvas ? canvas.toDataURL("image/png") : null;
    if (tempBackground) this.state.stage.removeChild(tempBackground);
    this.state.stage.update();
    return base64Image;
  }

  handleButtonPress() {
    this.buttonPressTimer = setTimeout(() => this.refresh(), 2000);
  }

  handleButtonRelease() {
    clearTimeout(this.buttonPressTimer);
  }

  render() {
    const self = this;

    const canvasHeight = 1000;
    const canvasWidth = canvasHeight / this.props.settings.canvas_ratio;

    return (
      <div id="art">
        <div
          id="hidden-refresh"
          onTouchStart={this.handleButtonPress}
          onTouchEnd={this.handleButtonRelease}
          onMouseDown={this.handleButtonPress}
          onMouseUp={this.handleButtonRelease}
          onMouseLeave={this.handleButtonRelease}
        />
        {this.state.modalOn && (
          <Modal
            message="Are you sure you want to submit?"
            buttons={this.modalButtons}
          />
        )}
        {this.state.exiting && (
          <Modal
            message="You will lose your drawing. Are you sure you want to exit?"
            buttons={this.exitButtons}
          />
        )}
        <div className="top-nav">
          <img
            className="close-icon"
            src={`${location.origin}/images/times.png`}
            onClick={this.exit.bind(this)}
          />
        </div>

        <div id="canvas-wrapper">
          <canvas id="canvas" width={canvasWidth} height={canvasHeight} />
        </div>

        <ArtTools
          // ideally this would live in redux
          stage={this.state.stage}
          paletteColors={this.props.settings.colors}
          activeColor={this.state.color}
          onClickPaletteColor={(color) => this.selectPaletteColor(color)}
          onClickUndo={() => this.undo()}
          toggleWeightSelector={() => this.toggleThicknessSelect()}
          weightSelectorActive={this.state.weightSelectorActive}
          changePenWeight={(thickness) => this.changeThickness(thickness)}
          cancelChangePenWeight={() => this.cancelChangePenWeight()}
          onClickToggleBackgroundColorSelect={() =>
            this.toggleBackgroundColorSelect()
          }
          backgroundSelectorActive={this.state.backgroundSelectorActive}
          clearBackground={() => this.addBackgroundColor(null)}
          submit={() => this.submit()}
        />
      </div>
    );
  }
}

export default Art;
