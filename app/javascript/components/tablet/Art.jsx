import React, { useEffect, useState, useContext } from "react";
import { Pen } from "./art/Pen";
import { SubmitModal } from "./art/SubmitModal";
import { ExitModal } from "./art/ExitModal";
import { TopNav } from "./art/TopNav";
import { BackgroundColor } from "./art/BackgroundColor";
import { ArtTools } from "./art/ArtTools";

import { Context, setStage } from "./TabletAppStore";
import { liveUpdateDrawing, liveUpdateReset } from "../../api/drawings";
import { ArtCanvas } from "./art/ArtCanvas";

import styled from "styled-components";

const ArtWrapper = styled.div`
  margin: 0 auto;
  height: 100%;
  justify-content: center;
  display: flex;
  flex-direction: column;
  width: 100%;
  overflow: hidden;
`;

const maxUndo = 15;
const canvasHeight = 1000;

const resetTimer = () => window.clearTimeout(window.idleTimeoutID);

export const Art = ({ settings, setPage, setDrawing }) => {
  const { state, dispatch } = useContext(Context);
  const { stage } = state;

  const [wasDrawing, setWasDrawing] = useState(false);
  const [snapshots, setSnapshots] = useState([]);

  const [modalOn, setModalOn] = useState(false);
  const [exiting, setExiting] = useState(false);
  const [backgroundColor, setBackgroundColor] = useState(null);
  const [penColor, setPenColor] = useState(null);
  const [penWeight, setPenWeight] = useState(30);
  const [backgroundSelectorActive, setBackgroundSelectorActive] =
    useState(false);
  const [weightSelectorActive, setWeightSelectorActive] = useState(false);
  const [ipad, setIpad] = useState(null);
  const [pen, setPen] = useState(null);

  useEffect(() => {
    // When the Art screen loads, initialize Easel.js on the canvas
    // and pass it to the store so that other components can access it
    var stage = new window.createjs.Stage("canvas");
    dispatch(setStage(stage));
    window.createjs.Touch.enable(stage, true, false);
    setIpad(window.getUrlParam("ipad", "0"));
    startTimer();

    // Remove this setTimeout when unmounting
    return () => {
      resetTimer();
    };
  }, []);

  useEffect(() => {
    if (wasDrawing) {
      const newSnapshot = convertCanvasToBase64();
      const nextSnapshots = [...snapshots, newSnapshot];

      if (stage.numChildren >= maxUndo) {
        const oldestSnapshot = nextSnapshots.shift();
        applySnapshot(oldestSnapshot);
      }

      setWasDrawing(false);
      setSnapshots(nextSnapshots);
    }
  }, [wasDrawing]);

  // When the background color changes, update the live drawing
  useEffect(() => {
    updateActiveWallDrawing();
  }, [backgroundColor]);

  // Once the stage is initialized, give the user the default pen
  useEffect(() => {
    if (stage && stage.update) {
      stage.update();
      const color = settings.colors[settings.colors.length - 1];
      setPenColor(color);
    }
  }, [stage]);

  // Any time the color or thickness of the pen changes, give the user a new pen
  useEffect(() => {
    if (penColor && penWeight && stage) changePen();
  }, [penColor, penWeight]);

  // A "Snapshot" seems to be a way to reduce all art shapes into one
  // This allows us to limit the amount of shapes rendered on the canvas
  // to however many we want the user to be able to undo.
  //
  // Since snapshots are created whenever an art shape is added and
  // removed whenever an undo operation is successful or the list of snapshots
  // exceeds the maximum undo depth, snapshot changes can be used as a trigger
  // to update the live drawing
  useEffect(() => {
    updateActiveWallDrawing();
  }, [snapshots]);

  const cancelChangePenWeight = () => {
    setWeightSelectorActive(false);
  };

  const submit = () => {
    const image = convertCanvasToBase64(true);
    setDrawing({ image }, true);
    if (settings.signup_screen) setPage("NewUser");
    else setPage("ThankYou");
  };

  const exit = () => {
    liveUpdateReset(settings["project_id"], ipad)
      .then((res) => {
        console.log(res);
        setPage("Start");
      })
      .catch((e) => {
        console.error(e);
        setPage("Start");
      });
  };

  const toggleModal = () => {
    setModalOn(!modalOn);
  };

  const toggleExitModal = () => {
    setExiting(!exiting);
  };

  const applySnapshot = (snapshot) => {
    const bm = new window.createjs.Bitmap(snapshot);
    bm.type = "snapshot";
    bm.createdAt = new Date();
    bm.image.onload = () => {
      stage.removeChildAt(0);
      stage.addChildAt(bm, 0);
      if (stage.numChildren > maxUndo) {
        var oldestChild = stage.children.find((child, index) => {
          if (child.type !== "snapshot") child.arrayPosition = index;
          return child.type !== "snapshot";
        });
      }
      if (oldestChild) stage.removeChildAt(oldestChild.arrayPosition);
      stage.update();
    };
  };

  const updateActiveWallDrawing = () => {
    const image = convertCanvasToBase64(true);
    liveUpdateDrawing(settings["project_id"], image, ipad);
    resetTimer();
  };

  const changePen = () => {
    if (!stage) return;
    if (pen) pen.destroy();

    const onPenUp = () => {
      setWasDrawing(true);
    };
    const onPenDestroy = () => setPen(null);

    setPen(Pen(stage, penColor, penWeight, onPenUp, onPenDestroy));
    setBackgroundSelectorActive(false);
    setWeightSelectorActive(false);
  };

  const addBackgroundColor = (color) => {
    const canvas = document.getElementById("canvas");
    canvas.style.backgroundColor = color;
    setBackgroundSelectorActive(false);
    setBackgroundColor(color);
  };

  const selectPaletteColor = (color) => {
    if (backgroundSelectorActive) addBackgroundColor(color);
    else setPenColor(color);
  };

  // the withBackgroundColor param is used to prevent snapshots from having
  // the background color applied
  const convertCanvasToBase64 = (withBackgroundColor) => {
    if (!stage) return;
    let tempBackground = null;
    if (withBackgroundColor && backgroundColor) {
      tempBackground = BackgroundColor(stage, backgroundColor);
    }
    const canvas = document.getElementById("canvas");
    const base64Image = canvas ? canvas.toDataURL("image/png") : null;
    if (tempBackground) stage.removeChild(tempBackground);
    stage.update();
    return base64Image;
  };

  const startTimer = () => {
    window.idleTimeoutID = window.setTimeout(exit, 60000);
  };

  const changePenWeight = (weight) => {
    if (!weight) weight = penWeight;
    setPenWeight(weight);
  };

  const undo = () => {
    if (stage.numChildren > 0) {
      let lastChild = stage.children[stage.children.length - 1];
      if (lastChild.type !== "snapshot") {
        stage.removeChild(lastChild);
        setSnapshots(snapshots.slice(0, snapshots.length - 1));
        stage.update();
      }
    }
  };

  const toggleThicknessSelect = () => {
    setWeightSelectorActive(!weightSelectorActive);
    setBackgroundSelectorActive(
      weightSelectorActive ? false : backgroundSelectorActive
    );
  };

  const toggleBackgroundColorSelect = () => {
    setBackgroundSelectorActive(!backgroundSelectorActive);
    setWeightSelectorActive(
      backgroundSelectorActive ? false : weightSelectorActive
    );
  };

  const canvasWidth = canvasHeight / settings.canvas_ratio;

  return (
    <ArtWrapper id="art-wrapper">
      {modalOn && <SubmitModal toggleModal={toggleModal} submit={submit} />}
      {exiting && <ExitModal toggleModal={toggleExitModal} exit={exit} />}

      <TopNav id="top-nav" exit={exit} />

      <ArtCanvas id="art-canvas-wrapper" width={canvasWidth} height={canvasHeight} />

      <ArtTools
        id="art-tools-wrapper"
        paletteColors={settings.colors}
        activeColor={penColor}
        onClickPaletteColor={selectPaletteColor}
        onClickUndo={undo}
        toggleWeightSelector={toggleThicknessSelect}
        weightSelectorActive={weightSelectorActive}
        changePenWeight={changePenWeight}
        cancelChangePenWeight={cancelChangePenWeight}
        onClickToggleBackgroundColorSelect={toggleBackgroundColorSelect}
        backgroundSelectorActive={backgroundSelectorActive}
        clearBackground={() => addBackgroundColor(null)}
        submit={submit}
      />
    </ArtWrapper>
  );
};
