import React from "react";

class AvailableLanguage extends React.Component {
  getClassName() {
    // console.log(this.props.active, this.props.language)
    return this.props.active ? "language active" : "language";
  }

  render() {
    return (
      // <option className={this.getClassName()} value={this.props.language}>{this.props.language}</option>
      <li
        className={this.getClassName()}
        value={this.props.language}
        onClick={() => this.props.changeLanguage(this.props.language, true)}
      >
        {this.props.language}
      </li>
    );
  }
}

export default AvailableLanguage;
