import React from "react";
import Modal from "./Modal";
const TermsParagraphs = require("../../TermsParagraphs");

class TermsModal extends Modal {
  render() {
    let paragraphs = TermsParagraphs.map((p, index) => {
      return <p key={index}>{p}</p>;
    });

    return (
      <div id="modal">
        <div id="modal-content">
          <h1>PROMOTIONAL DRAWING TERMS &amp; CONDITIONS</h1>
          <div id="terms-container">{paragraphs}</div>
          <div id="term-buttons">
            <div id="modal-buttons">
              <button className="button" onClick={this.props.toggleModal}>
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TermsModal;
