import React from "react";
import styled from "styled-components";

// TODO: make these things shared (this is copied from dashboard/theme.js)
export const FlexColumn = styled.div`
  display: flex;
`;

export const BorderedContainer = styled(FlexColumn)`
  border-right: 2px solid lightgray;
`;

export const ToolButtonWrapper = styled.div`
  width: 5em;
  height: 5em;
  padding: 1em;
`;

export const ToolButton = styled.div`
  width: 100%;
  height: 100%;
  background-color: #ffffff;
  background-size: 60%;
  background-repeat: no-repeat;
  background-position: center;
  position: relative;
`;

