import React from "react";
import styled from "styled-components";

const NavContainer = styled.div`
  display: flex;
  align-items: end;
  justify-content: end;
  padding: 1rem 2rem 0px;
`;

const CloseButton = styled.div`
  background-repeat: no-repeat;
  width: 36px;
  height: 36px;
  position: relative;
  top: 1rem;
`;

export const TopNav = ({ exit }) => (
  <NavContainer id="close-button-container">
    <CloseButton id="close-button" onClick={exit} />
  </NavContainer>
);
