import React from "react";
import styled from "styled-components";

const CanvasWrapper = styled.div`
  position: relative;
  margin: 0.5rem 7rem;

  canvas {
    display: block;
    background: #ffffff;
    margin: 0 auto;
    width: 90%;
    border: 2px solid #999999;
  }
`;

export const ArtCanvas = ({ width, height }) => {
  return (
    <CanvasWrapper id="canvas-wrapper">
      <canvas id="canvas" width={width} height={height} />
    </CanvasWrapper>
  );
};
