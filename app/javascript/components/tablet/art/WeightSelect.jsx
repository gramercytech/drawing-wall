import React from "react";
import styled from "styled-components";
import { ToolButton, ToolButtonWrapper } from "./ArtTheme";
import { Tooltip } from "./Tooltip";

const WeightSelectButton = styled(ToolButton)`
  background-size: 75%;
`;

const SelectWeight = styled.div`
  display: flex;
  flex-basis: 4rem;
  justify-content: center;
  align-items: center;
  padding: 1rem;
`;

const Weight = styled.div`
  border-radius: 50%;
  background-color: #000000;
  width: ${(props) => props.size};
  height: ${(props) => props.size};
`;

const Weights = ({ stage, changePenWeight, cancelChangePenWeight }) => (
  <Tooltip 
    stage={stage}
    onClickAway={cancelChangePenWeight}
    className="weight-selector-tooltip"
    // styles={"grid-template-rows: repeat(4, 50px);"}
  >
    <SelectWeight className="weight-selector-option-container" onClick={() => changePenWeight(50)}>
      <Weight className="weight-selector-option option-375" size={"3.75rem"} />{" "}
    </SelectWeight>
    <SelectWeight className="weight-selector-option-container" onClick={() => changePenWeight(30)}>
      <Weight className="weight-selector-option option-275" size={"2.75rem"} />{" "}
    </SelectWeight>
    <SelectWeight className="weight-selector-option-container" onClick={() => changePenWeight(20)}>
      <Weight className="weight-selector-option option-175" size={"1.75rem"} />{" "}
    </SelectWeight>
    <SelectWeight className="weight-selector-option-container" onClick={() => changePenWeight(10)}>
      <Weight className="weight-selector-option option-125" size={"1.25rem"} />{" "}
    </SelectWeight>
  </Tooltip>
);

export const WeightSelect = ({
  stage,
  toggleWeightSelector,
  weightSelectorActive,
  changePenWeight,
  cancelChangePenWeight,
}) => (
  <ToolButtonWrapper 
    className="weight-selector-column">
    <WeightSelectButton
      id="weight-select"
      onClick={() => toggleWeightSelector()}
    >
      {weightSelectorActive && (
        <Weights 
          className="weight-selector-tooltip-container"
          stage={stage}
          changePenWeight={changePenWeight}
          cancelChangePenWeight={cancelChangePenWeight}
        />
      )}
    </WeightSelectButton>
  </ToolButtonWrapper>
);
