import React from "react";
import styled from "styled-components";
import { Colors } from "../../dashboard/theme";
import { ToolButton, ToolButtonWrapper } from "./ArtTheme";
import { Tooltip } from "./Tooltip";

const BackgroundColorSelectButton = styled(ToolButton)`
  background: ${(props) => (props.active ? Colors.diesel : Colors.white)};
  color: ${(props) => (props.active ? Colors.white : Colors.diesel)};
  font-size: xxx-large;
  line-height: 1em;
`;

const ClearSymbol = styled.div`
  font-size: 2.5em;
`;

const ClearBackgroundButton = ({ stage, onClick, onClickAway }) => (
  <Tooltip id="clear-bg-tooltip" stage={stage} onClickAway={onClickAway}>
    <ClearSymbol id="clear-bg-icon" onClick={onClick}>🚫</ClearSymbol>
  </Tooltip>
);

export const BackgroundColorSelectToggle = ({
  // to do: this is available from the store now
  stage,
  isActive,
  toggleBackgroundSelector,
  clearBackground,
}) => (
  <ToolButtonWrapper className="tool-button-wrapper">
    <BackgroundColorSelectButton
      id="background-select-toggle"
      active={isActive}
      onClick={toggleBackgroundSelector}
    >
      <span>&#9639;</span>
      {isActive && (
        <ClearBackgroundButton
          id="clear-bg-btn-container"
          stage={stage}
          onClick={clearBackground}
          onClickAway={toggleBackgroundSelector}
        />
      )}
    </BackgroundColorSelectButton>
  </ToolButtonWrapper>
);
