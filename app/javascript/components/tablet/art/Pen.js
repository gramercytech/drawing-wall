import { v4 as uuidv4 } from "uuid";

export const Pen = (stage, color, thickness, callback, onDestroy) => {
  var x = 0;
  var y = 0;
  var mouseMoveListener;
  var mouseUpListener;
  const hex = parseInt("0x" + color.substring(1));
  var id = uuidv4();
  // I wouldn't mind leaving this logging in for a little bit
  // to help assure we never end up with multiple pens
  console.log(`Pen<${id}, ${color}> created`);
  var color = window.createjs.Graphics.getRGB(hex);
  var art;

  var startDraw = (evt) => {
    // console.log(`Pen<${id}, ${color}> drawing`);
    art = stage.addChild(new window.createjs.Shape());
    art.createdAt = new Date();
    art.category = "art";
    art.cache(0, 0, stage.canvas.width, stage.canvas.height);
    mouseMoveListener = stage.on("stagemousemove", draw, this);
    mouseUpListener = stage.on("stagemouseup", endDraw, this);
    x = evt.stageX - 0.001;
    y = evt.stageY - 0.001;
    draw(evt);
  };

  var stageMouseDownListener = stage.on("stagemousedown", startDraw, this);

  function draw(evt) {
    art.graphics
      .setStrokeStyle(thickness, 1)
      .beginStroke(color)
      .moveTo(x, y)
      .lineTo(evt.stageX, evt.stageY);
    art.updateCache("source-over");
    art.graphics.clear();
    x = evt.stageX;
    y = evt.stageY;
    stage.setChildIndex(art, stage.children.length - 1);
    stage.update();
  }

  function endDraw(evt) {
    stage.off("stagemousemove", mouseMoveListener);
    evt.remove();
    callback();
  }

  var pen = {
    destroy: () => {
      console.log(`Pen<${id}, ${color}> destroyed`);
      stage.off("stagemousedown", stageMouseDownListener);
      stage.off("stagemousemove", mouseMoveListener);
      stage.off("stagemouseup", mouseUpListener);
      document.body.style.cursor = "default";
      if (onDestroy) onDestroy();
    },
    hex: hex,
  };

  return pen;
};
