import React, { useEffect, useRef, useCallback } from "react";
import styled from "styled-components";

// TODO get more of these colors etc importing from a theme file
const TooltipWrapper = styled.div`
  background-color: #ffffff;
  border-radius: 5px;
  bottom: 5em;
  bottom: 80px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.4);
  color: #ffffff;
  font-size: 12px;
  font-family: "Helvetica", sans-serif;
  left: -1.5em;
  padding: 7px 1.5em;
  position: absolute;
  width: 80px;
  z-index: 4;

  &:after {
    border-top: 12px solid #ffffff;
    border-right: 9px solid transparent;
    border-left: 9px solid transparent;
    bottom: -9px;
    content: "";
    display: block;
    left: 50%;
    margin-left: -9px;
    position: absolute;
    z-index: 9999;
  }

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-items: center;

  -webkit-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
  -moz-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;
  -ms-animation: fade-in 0.3s linear 1, move-up 0.3s linear 1;

  ${(props) => (props.styles ? `${props.styles}` : ``)}
`;

export const Tooltip = ({ stage, onClickAway, children, styles }) => {
  const tooltipContainer = useRef();

  const clickAwayListener = (event) => {
    const clickingOnTooltip = event.target != tooltipContainer.current;
    const clickingOnTooltipParent =
      event.target != tooltipContainer.current.parentElement;
    if (!clickingOnTooltip && !clickingOnTooltipParent)
      onClickAway(event.target);
  };

  const clickStageListener = (event) => {
    onClickAway(event.target);
  };

  useEffect(() => {
    document.body.addEventListener("click", clickAwayListener);
    stage.on("stagemousedown", clickStageListener, clickStageListener, true);

    return () => {
      document.body.removeEventListener("click", clickAwayListener);
      stage.off("stagemousedown", clickStageListener, clickStageListener, true);
    };
  }, [tooltipContainer, stage]);

  return (
    <TooltipWrapper ref={tooltipContainer} styles={styles}>
      {children}
    </TooltipWrapper>
  );
};
