import React from "react";
import styled from "styled-components";
import { ToolButton, ToolButtonWrapper } from "./ArtTheme";

const SubmitButton = styled(ToolButton)`
  align-self: flex-end;
  background-size: 80%;
  margin-right: 10px;
`;

export const Submit = ({ onClickSubmit }) => (
  <ToolButtonWrapper className="column">
    <SubmitButton id="submit-btn" onClick={onClickSubmit} />
  </ToolButtonWrapper>
);
