import React from "react";
import { PaletteColor } from "./PaletteColor";
import { FlexColumn } from "./ArtTheme";
import styled from "styled-components";

const PaletteWrapper = styled(FlexColumn)`
    flex-grow: 1;
    display: grid;
    /* justify-content: center; */
    padding: 0 1em;
    grid-auto-flow: column;
    align-items: center;
`;

export const Palette = ({
  paletteColors,
  activeColor,
  onClickPaletteColor,
}) => (
  <PaletteWrapper className="palette-wrapper">
    {paletteColors.map((color) => (
      <PaletteColor
        className="color-wrapper"
        key={color}
        color={color}
        onClick={onClickPaletteColor}
        active={color == activeColor}
      />
    ))}
  </PaletteWrapper>
);
