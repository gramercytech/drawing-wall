import React, { useContext } from "react";
import { Context } from "../TabletAppStore";
import { BackgroundColorSelectToggle } from "./BackgroundColorSelectToggle";
import { WeightSelect } from "./WeightSelect";
import { BorderedContainer } from "./ArtTheme";
import { UndoButton } from "./UndoButton";
import { Palette } from "./Palette";
import { Submit } from "./Submit";
import styled from "styled-components";

const ArtToolsWrapper = styled.div`
  background: #e8e1d4;
  border-top: 1px solid #ffffff;
  /* position: absolute; */
  /* bottom: 0; */
  width: 100%;
  display: flex;
  padding-bottom: 1rem;
`;

export const ArtTools = ({
  paletteColors,
  activeColor,
  onClickPaletteColor,
  onClickUndo,
  toggleWeightSelector,
  weightSelectorActive,
  changePenWeight,
  cancelChangePenWeight,
  onClickToggleBackgroundColorSelect,
  backgroundSelectorActive,
  clearBackground,
  submit,
}) => {
  const { state, dispatch } = useContext(Context);
  // this is just a proof of concept, there should be more ways
  // to make useContext useful
  const { stage } = state;

  return (
    <ArtToolsWrapper id="art-tools-wrapper">
      <UndoButton id="undo-btn" onClickUndo={onClickUndo} />
      <BorderedContainer className="weight-background-column">
        <WeightSelect
          id="pen-btn"
          stage={stage}
          toggleWeightSelector={toggleWeightSelector}
          weightSelectorActive={weightSelectorActive}
          changePenWeight={changePenWeight}
          cancelChangePenWeight={cancelChangePenWeight}
        />
        <BackgroundColorSelectToggle
          id="paint-bucket-btn"
          stage={stage}
          toggleBackgroundSelector={onClickToggleBackgroundColorSelect}
          isActive={backgroundSelectorActive}
          clearBackground={clearBackground}
        />
      </BorderedContainer>
      <Palette
        id="color-palette-container"
        paletteColors={paletteColors}
        activeColor={activeColor}
        onClickPaletteColor={onClickPaletteColor}
      />
      <Submit id="art-tools-submit-btn" onClickSubmit={submit} />
    </ArtToolsWrapper>
  );
};
