import React from "react";
import Modal from "../Modal";

export const SubmitModal = ({ toggleModal, submit }) => (
  <Modal
    message="Are you sure you want to submit?"
    buttons={[
      { label: "Back", action: toggleModal, class: "button-outline" },
      { label: "Submit", action: submit },
    ]}
  />
);
