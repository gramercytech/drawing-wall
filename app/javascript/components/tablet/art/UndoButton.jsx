import React from "react";
import { BorderedContainer, ToolButtonWrapper, ToolButton } from "./ArtTheme";

export const UndoButton = ({ onClickUndo }) => (
  <BorderedContainer className="undo-button-column" onClick={onClickUndo}>
    <ToolButtonWrapper className="undo-button-wrapper">
      <ToolButton id="undo-button" />
    </ToolButtonWrapper>
  </BorderedContainer>
);
