import React from "react";
import { FlexColumn } from "./ArtTheme";
import styled from "styled-components";

const ColorWrapper = styled(FlexColumn)`
  align-items: center;
  justify-content: center;
  width: 5.5rem;
  height: 5.5rem;
`;

const ColorSelect = styled.div`
  border-radius: 50%;
  border: 1px solid #000000;
  height: ${(props) => (props.active ? "5.5rem" : "4rem")};
  width: ${(props) => (props.active ? "5.5rem" : "4rem")};
  background: ${(props) => props.background};
`;

export const PaletteColor = ({ color, onClick, active }) => (
  <ColorWrapper className="palette-color">
    <ColorSelect className={`color color-${color}`}
      active={active}
      background={color}
      onClick={() => onClick(color)}
    />
  </ColorWrapper>
);
