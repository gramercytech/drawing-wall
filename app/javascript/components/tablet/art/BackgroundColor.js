export const BackgroundColor = (stage, color) => {
  const x = 0;
  const y = 0;
  const hex = parseInt("0x" + color.substring(1));
  const rgb = window.createjs.Graphics.getRGB(hex);
  const canvas = document.getElementById("canvas");
  const shape = stage.addChild(new window.createjs.Shape());
  shape.createdAt = new Date();
  shape.category = "background";
  shape.cache(0, 0, stage.canvas.width, stage.canvas.height);
  shape.graphics.beginFill(rgb).drawRect(0, 0, canvas.width, canvas.height);
  shape.updateCache("source-over");
  shape.graphics.clear();
  stage.setChildIndex(shape, 0);
  stage.update();
  return shape
};
