import React from "react";
import Modal from "../Modal";

export const ExitModal = ({ toggleModal, exit }) => (
  <Modal
    message="You will lose your drawing. Are you sure you want to exit?"
    buttons={[
      { label: "Yes", action: exit },
      { label: "No", action: toggleModal, class: "button-outline" },
    ]}
  />
);
