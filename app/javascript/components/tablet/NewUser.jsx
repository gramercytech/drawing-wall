import React from "react";
import Modal from "./Modal";
import TermsModal from "./TermsModal";
// const Countries = require('../Countries')

class NewUser extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleBackModal = this.toggleBackModal.bind(this);
    this.backMessage =
      "You will lose your drawing. Are you sure you want to start over?";
    this.backButtons = [
      {
        label: "Yes",
        action: () => {
          this.props.setPage("Start");
        },
      },
      {
        label: "No",
        action: this.toggleBackModal.bind(this),
        class: "button-outline",
      },
    ];
    this.state = {
      modalOn: false,
      goingBack: false,
      nameRequired: false,
    };
  }

  toggleModal(e) {
    e.preventDefault();
    const modalOn = !this.state.modalOn;
    this.setState({ modalOn });
  }

  toggleBackModal(e) {
    e.preventDefault();
    const goingBack = !this.state.goingBack;
    this.setState({ goingBack });
  }

  handleSubmit(event) {
    event.preventDefault();
    const formData = new window.FormData(event.target);
    var user = {};
    formData.forEach((value, key) => {
      user[key] = value;
    });
    user.accepts_terms = document.getElementById("accepts-terms").checked;
    this.props.setDrawing(user, true);
    window.drawing = null;
    this.props.setPage("ThankYou");
  }

  toggleRequiredFields() {
    let nameRequired = !this.state.nameRequired;
    this.setState({ nameRequired });
  }

  render() {
    return (
      <div id="new-user-form">
        {this.state.goingBack && (
          <Modal message={this.backMessage} buttons={this.backButtons} />
        )}
        {this.state.modalOn && (
          <TermsModal toggleModal={this.toggleModal.bind(this)} />
        )}
        <h2>Enter for a chance to win a ticket to GramWorld 2019 US!</h2>
        <p>
          Entering the raffle is optional, but we will need your email address
          <br />
          to notify you if you've won. We will not send you any spam
          <br />
          or share your email address with any third parties.
          <br />
          To submit anonymously, simply click 'Continue'
        </p>
        <form onSubmit={this.handleSubmit}>
          <fieldset>
            <label htmlFor="email">
              E-mail &nbsp;
              {this.state.nameRequired && (
                <span className="required-indicator">*</span>
              )}
            </label>
            <input
              type="email"
              id="email"
              name="email"
              required={this.state.nameRequired}
            />
          </fieldset>

          <div className="terms-container">
            <div className="term-button i-agree">
              <input
                type="checkbox"
                id="accepts-terms"
                name="accepts-terms"
                onChange={() => this.toggleRequiredFields()}
              />
              <label className="label-inline" htmlFor="accepts-terms">
                I agree to the Terms and Conditions.
              </label>
            </div>
          </div>

          <div className="new-user-buttons">
            <button className="button" type="submit">
              Continue
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default NewUser;
