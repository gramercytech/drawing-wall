import React from "react";

class Start extends React.Component {
  render() {
    const bg = this.props.settings.start_screen.image;

    const startStyle = {
      backgroundImage: `url(${bg})`,
    };

    const buttonStyle = {
      padding: 0,
      width: this.props.settings.start_screen.button_width,
      height: this.props.settings.start_screen.button_height,
      marginTop: this.props.settings.start_screen.button_y,
      opacity: this.props.settings.start_screen.button_opacity,
    };

    return (
      <div id="start" className="centerer" style={startStyle}>
        <div className="centerer">
          <div className="logo" />
          <div className="button-container">
            <button
              style={buttonStyle}
              onClick={() => this.props.setPage("Art")}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Start;
