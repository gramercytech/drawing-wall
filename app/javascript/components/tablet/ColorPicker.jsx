import React from "react";

class ColorPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ctx: null,
    };
  }

  componentDidMount() {
    var canvas = document.getElementById("picker");
    var ctx = canvas.getContext("2d");
    this.setState({ ctx });
    var image = new window.Image();
    image.onload = function () {
      ctx.drawImage(image, 0, 0, image.width, image.height);
    };
    image.src = "../images/rainbow.png";
  }

  setActiveColor(e) {
    var picker = e.target;
    var canvasOffset = {
      top: picker.offsetTop,
      left: picker.offsetLeft,
    };
    var ratio = picker.width / picker.getBoundingClientRect().width;
    var canvasX = Math.floor(e.pageX - canvasOffset.left);
    var canvasY = Math.floor(e.pageY - canvasOffset.top);
    var ctx = this.state.ctx;
    var imageData = ctx.getImageData(canvasX * ratio, canvasY * ratio, 1, 1);
    var pixel = imageData.data;

    var pixelColor =
      "rgb(" + pixel[0] + ", " + pixel[1] + ", " + pixel[2] + ")";

    var red = pixel[0];
    var green = pixel[1];
    var blue = pixel[2];
    var dColor = blue + 256 * green + 65536 * red;
    this.props.changePen(dColor);
    document.querySelector("#preview-tab-color").style.backgroundColor =
      pixelColor;
    document.querySelector(".preview").style.left = canvasX - 5 + "px";
  }

  render() {
    return (
      <div id="color-picker">
        <div className="colorpicker">
          <canvas
            id="picker"
            width="2428"
            height="72"
            onClick={(e) => this.setActiveColor(e)}
          />
        </div>
        <div className="preview">
          <img src="../images/color-tab.png" />
          <div id="preview-tab-color" />
        </div>
      </div>
    );
  }
}

export default ColorPicker;
