import React, { useEffect } from "react";
import QRCode from "react-qr-code";
import { baseProjectsPath } from "../../api/projects";
import styled from "styled-components";

const refresh = () => window.location.reload();
const zeroPxIfNotSet = (value) => (value ? value : "0px");

const ThankYouPageWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-image: ${(props) => `url(${props.backgroundImage})`};
  background-size: cover;
`;

const QrWrapper = styled.div`
  position: absolute;
  ${(props) => (props.top ? `top: ${props.top}` : ``)};
  ${(props) => (props.width ? `width: ${props.width}` : ``)};
`;

const FinishButton = styled.div`
  position: absolute;
  width: ${(props) => `${props.width}`};
  height: ${(props) => `${props.height}`};
  left: ${(props) => `${props.left}`};
  top: ${(props) => `${props.top}`};
  opacity: ${(props) => `${props.opacity}`};
  background: purple;
`;

export const ThankYou = ({ settings, drawing }) => {
  useEffect(() => {
    const refreshTimeoutId = setTimeout(refresh, 50000);

    return () => {
      clearTimeout(refreshTimeoutId);
    };
  }, []);

  const {
    // TODO fix the casing
    button_width,
    button_height,
    button_x,
    button_y,
    button_opacity,
    image,
    qr_code,
    qr_code_top,
    qr_code_width,
    qr_bg_color,
    qr_fg_color,
  } = settings.thankyou_screen;

  return (
    <ThankYouPageWrapper id="thank-you-wrapper" backgroundImage={image}>
      {qr_code && drawing && drawing.id && (
        <QrWrapper top={qr_code_top} width={qr_code_width} className="qr-wrapper">
          <QRCode
            className="qr-code"
            size={256}
            style={{ height: "auto", maxWidth: "100%", width: "100%" }}
            value={`${baseProjectsPath}/${drawing.projectId}/drawings/${drawing.id}`}
            viewBox={`0 0 256 256`}
            bgColor={qr_bg_color ? qr_bg_color : "#ffffff"} 
            fgColor={qr_fg_color ? qr_fg_color : "#000000"}
          />
        </QrWrapper>
      )}

      <FinishButton
        className="thank-you-finish-button"
        width={zeroPxIfNotSet(button_width)}
        height={zeroPxIfNotSet(button_height)}
        left={zeroPxIfNotSet(button_x)}
        top={zeroPxIfNotSet(button_y)}
        opacity={button_opacity ? button_opacity : "0"}
        onClick={refresh}
      />
    </ThankYouPageWrapper>
  );
};
