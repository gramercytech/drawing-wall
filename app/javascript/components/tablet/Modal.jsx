import React from "react";

class Modal extends React.Component {
  getButtonClass(value) {
    return value ? `button ${value}` : "button";
  }

  render() {
    let buttons = this.props.buttons.map((button, index) => {
      return (
        <button
          key={index}
          className={this.getButtonClass(button.class)}
          onClick={button.action}
        >
          {button.label}
        </button>
      );
    });

    return (
      <div id="modal">
        <div id="modal-content">
          <p>{this.props.message}</p>
          <div id="modal-buttons">{buttons}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
