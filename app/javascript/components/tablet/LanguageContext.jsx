export const Dictionary = {
  English: {
    selectedLang: "English",
    header: "Draw Amway’s Future",
    subHeader: "Share your ideas for future Amway products and innovations!",
    feedback: "What products would you like to see at A70?",
    you: "You",
  },
  български: {
    selectedLang: "български",
    header: "Оформете бъдещето на Amway",
    subHeader: "Споделете вашите идеи за бъдещи продукти и иновации на Amway!",
    feedback: "Какви продукти бихте искали да видите на А70?",
    you: "Вие",
  },
  Español: {
    selectedLang: "Español",
    header: "Traza el futuro de Amway",
    subHeader:
      "¡Comparte tus ideas sobre productos e innovaciones futuras de Amway!",
    feedback: "¿Qué productos te gustaría ver en A70?",
    you: "Tú",
  },
  "Español (EU)": {
    selectedLang: "Español (EU)",
    header: "Dibuja el futuro de Amway",
    subHeader:
      "¡Comparte tu ideas para productos e innovaciones futuras de Amway!",
    feedback: "¿Qué productos te gustaría ver en el A70?",
    you: "Tú",
  },
  // Simplified
  "中文 (简体字)": {
    selectedLang: "中文 (简体字)",
    header: "畅想安利的未来",
    subHeader: "分享您关于未来安利产品和创新的想法！",
    feedback: "您希望在A70庆典上看到什么产品？",
    you: "您",
  },
  // Traditional
  "中文 (繁體字)": {
    selectedLang: "中文 (繁體字)",
    header: "暢想安麗的未來",
    subHeader: "分享您關於安麗未來產品和創新的創意！",
    feedback: "您希望在安麗 70 週年時看到哪些產品？",
    you: "您",
  },
  Čeština: {
    selectedLang: "Čeština",
    header: "Načrtněte budoucnost Amway",
    subHeader: "Sdílejte své nápady na budoucí výrobky a inovace Amway!",
    feedback: "Jaké výrobky byste chtěli mít na oslavách 70. výročí Amway?",
    you: "Vy",
  },
  Deutsch: {
    selectedLang: "Deutsch",
    header: "Zeichnen Sie die Zukunft von Amway",
    subHeader:
      "Teilen Sie Ihre Ideen für zukünftige Amway Produkte und Innovationen!",
    feedback: "Welche Produkte würden Sie bei A70 gerne sehen?",
    you: "Sie",
  },
  Magyar: {
    selectedLang: "Magyar",
    header: "Rajzold le az Amway jövőjét!",
    subHeader: "Oszd meg ötleteidet a jövő Amway termékeiről és innovációiról!",
    feedback: "Milyen termékeket látnál szívesen az A70-en?",
    you: "Te",
  },
  "Bahasa Indonesia": {
    selectedLang: "Bahasa Indonesia",
    header: "Gambarlah Masa Depan Amway",
    subHeader:
      "Sampaikan ide-ide Anda untuk produk dan inovasi Amway masa depan!",
    feedback: "Produk apa yang ingin Anda lihat pada acara A70?",
    you: "Anda",
  },
  Italiano: {
    selectedLang: "Italiano",
    header: "Disegna il futuro di Amway",
    subHeader:
      "Condividi le tue idee sui prodotti futuri e le innovazioni di Amway!",
    feedback:
      "Quali prodotti vorresti vedere in occasione dei festeggiamenti per A70?",
    you: "Tu",
  },
  日本語: {
    selectedLang: "日本語",
    header: "アムウェイの未来を描く",
    subHeader:
      "未来のアムウェイ製品やイノベーションについてのアイデアをお寄せください！",
    feedback: "A70 で見たい製品はどのようなものですか？",
    you: "あなた",
  },
  "Қазақ тілі": {
    selectedLang: "Қазақ тілі",
    header: "Amway болашағын суреттеңіз",
    subHeader:
      "Келешектегі Amway өнімдері мен инновациялары туралы ойларыңызды бөлісіңіз!",
    feedback: "A70 іс-шарасында қандай өнімдерді көргіңіз келеді?",
    you: "Сіз",
  },
  한국어: {
    selectedLang: "한국어",
    header: "암웨이의 미래 그리기",
    subHeader: "미래 암웨이 제품과 혁신에 대한 리더님의 아이디어를 나눠주세요!",
    feedback: "A70에서 보고 싶은 제품은 무엇인가요?",
    you: "리더님",
  },
  Polski: {
    selectedLang: "Polski",
    header: "Narysuj przyszłość Amway",
    subHeader:
      "Podziel się swoimi pomysłami na nowe produkty i innowacje Amway!",
    feedback: "Jakie produkty chciałbyś zobaczyć podczas obchodów A70?",
    you: "Ty",
  },
  Português: {
    selectedLang: "Português",
    header: "Desenhe o futuro da Amway",
    subHeader:
      "Compartilhe suas ideias para futuros produtos e inovações da Amway!",
    feedback: "Que produtos você gostaria de ver no A70?",
    you: "Você",
  },
  Română: {
    selectedLang: "Română",
    header: "Construiește viitorul Amway",
    subHeader: "Transmite-ți ideile pentru viitoare produse și inovații Amway!",
    feedback: "Ce produse ai vrea să vedeți la A70?",
    you: "Tu",
  },
  Pу́сский: {
    selectedLang: "Pу́сский",
    header: "Откройте будущее Amway",
    subHeader:
      "Поделитесь своими идеями о будущих продуктах и инновациях Amway!",
    feedback: "Какие продукты вы хотели бы увидеть на праздновании юбилея A70?",
    you: "Уважаемый (ая)",
  },
  Slovenščina: {
    selectedLang: "Slovenščina",
    header: "Narišite Amwayevo prihodnost",
    subHeader:
      "Delite svoje zamisli za prihodnje Amwayeve izdelke in inovacije!",
    feedback: "Katere izdelke bi radi videli na A70?",
    you: "Vi",
  },
  ภาษาไทย: {
    selectedLang: "ภาษาไทย",
    header: "อธิบายให้เห็นภาพอนาคตของแอมเวย์",
    subHeader:
      "แบ่งปันแนวคิดของท่านเกี่ยวกับผลิตภัณฑ์และนวัตกรรมของแอมเวย์ในอนาคต!",
    feedback: "ผลิตภัณฑ์ใดที่ท่านต้องการเห็นที่งานแอมเวย์ 70 ปี",
    you: "ท่าน",
  },
  Türkçe: {
    selectedLang: "Türkçe",
    header: "Amway’in Geleceğini Çizin",
    subHeader:
      "Geleceğe dönük Amway ürünleri ve inovasyonları için fikirlerinizi paylaşın!",
    feedback: "A70’te ne gibi ürünler görmek istersiniz?",
    you: "Siz",
  },
  Українська: {
    selectedLang: "Українська",
    header: "Створіть майбутнє Amway",
    subHeader:
      "Поділіться своїми ідеями щодо майбутніх продуктів та інновацій Amway.",
    feedback: "Які продукти ви хочете бачити на момент святкування А70?",
    you: "Ви",
  },
  "Tiếng Việt": {
    selectedLang: "Tiếng Việt",
    header: "Vẽ lên tương lai của Amway",
    subHeader:
      "Hãy chia sẻ ý tưởng của bạn cho các sản phẩm và cải tiến của Amway trong tương lai!",
    feedback: "Bạn muốn sản phẩm gì sẽ xuất hiện ở A70?",
    you: "Bạn",
  },
};

export const TranslatableText = (props) =>
  Dictionary[props.language][props.section];
