import React from 'react'
import GridSection from './wall/GridSection'
import { injectStyles } from '../InjectStyles'
import styled from 'styled-components'

const Drawings = styled.div`
  font-family: Gotham;
  display: grid;
  grid-template-columns: repeat(${props => props.gridSectionCount}, 1fr);
  grid-template-rows: 1fr;
  width: 100%;
  height: 100%;
  background-size: ${props => props.backgroundSize};
  background-repeat: ${props => props.backgroundRepeat};
  background-image: ${props => props.backgroundImage};
`

const DrawingLineup = require('../DrawingLineup')
// TODO: move into config
const smallCount = 46
const mediumCount = 0
const largeCount = 0

const Array_ = Array
Array_.prototype.inArray = function (comparer) {
  for (var i = 0; i < this.length; i++) {
    if (comparer(this[i])) return true
  }
  return false
}

Array_.prototype.pushIfNotExistOrUpdate = function (element, comparer) {
  if (!this.inArray(comparer)) {
    this.push(element)
  }
  const index = this.findIndex(drawing => drawing.id === element.id)
  this[index].hidden = element.hidden
}

class Wall extends React.Component {
  constructor (props) {
    super(props)
    this.latestDrawing = React.createRef()
    this.gridSection = React.createRef()

    this.gridSections = []
    for (let i = 0; i < props.config.grid_section_count; i++) {
      this.gridSections[i] = React.createRef()
    }

    // window.AllDrawings = this
    this.state = {
      sections: [],
      dupImages: null,
    }
  }

  removeDrawingById (id) {
    this.gridSections.forEach((section) => {
      if (section) section.current.removeDrawingById(id)
    })
  }

  showDrawingById (id) {
    this.gridSections.forEach((section) => {
      if (section) section.current.showDrawingById(id)
    })
  }

  updateLiveDrawing (data) {
    this.gridSections[data.ipad].current.updateLiveDrawing(data)
  }

  resetLiveDrawing (data) {
    this.gridSections[data.ipad].current.updateLiveDrawing(null)
  }

  componentDidMount () {
    const self = this
    window.App.messages = window.App.cable.subscriptions.create({
      channel: 'ProjectsChannel',
      project_id: this.props.config.project_id
    }, {
      connected: function () {
        console.log(`Connected to project/${self.props.config.project_id}`)
      },
      disconnected: function () {
        console.log(`Disconnected to project/${self.props.config.project_id}`)
      },
      rejected: function () {
        console.log(`Rejected from project/${self.props.config.project_id}`)
      },
      received: (data) => {
        switch (data.type) {
          case 'update':
            console.log('drawing update')
            // self.liveDrawing.current.update(data.drawing)
            self.updateLiveDrawing(data)
            break
          case 'complete':
            console.log('drawing complete', data)
            // self.liveDrawing.current.update(null)
            self.resetLiveDrawing(data)
            const src = data.drawing.image.url
            self.replaceOldestWith({ src, id: data.drawing.id, ipad: data.ipad })
            break
          case 'admin-update':
            console.log('admin-update', data)
            self.potentiallyAddToDrawings(data.drawing)
            if (data.drawing.hidden) {
              self.removeDrawingById(data.drawing.id)
            } else {
              self.showDrawingById(data.drawing.id)
            }
            break
          case 'reset':
            console.log('drawing reset')
            // self.liveDrawing.current.update(null)
            self.resetLiveDrawing(data)
            break
          default:
            break
        }
      }
    })
    var dupImages = window.getUrlParam('fill', '0')
    this.setState({ dupImages }, () => {
      var sections = this.distributeDrawings()
      this.createGridSections(sections)
      this.assembleLineup()
    })
    injectStyles(this.props.config.styles)
  }

  potentiallyAddToDrawings (drawing) {
    this.props.drawings.pushIfNotExistOrUpdate(drawing, function (e) {
      return e.id === drawing.id
    })
  }

  assembleLineup () {
    setTimeout(() => {
      window.lineup = DrawingLineup()
    }, 500)
  }

  replaceOldestWith (drawing) {
    window.lineup.rotateOut(drawing)
  }

  getCapacity () {
    const lives = this.props.config.live_columns * this.props.config.live_rows
    const others = this.props.config.grid_section_columns * this.props.config.grid_section_rows
    return others - lives
  }

  distributeDrawings () {
    const completedSections = []
    const sections = []
    const self = this
    const capacity = this.getCapacity()
    for (var i = 0; i < this.props.config.grid_section_count; i++) {
      sections.push({ component: 'GridSection', drawings: [], capacity })
    }
    // drawings?fill=1 to duplicate images for every section
    if (this.state.dupImages === '1') {
      sections.forEach(function (section) {
        section.drawings = self.props.drawings
      })
    } else {
      this.props.drawings.forEach(function (drawing) {
        if (drawing.hidden) return
        const sectionNumber = Math.floor(Math.random() * sections.length)
        const section = sections[sectionNumber]
        section.drawings.push({ image: drawing.image.url, id: drawing.id })
      })
    }
    return sections.concat(completedSections)
  }

  createGridSections (sections) {
    sections = sections.filter((section) => { return section.component === 'GridSection' })
    const gridSections = sections.map((section, index) => {
      return <GridSection
        key={index}
        dupImages={this.state.dupImages}
        sectionIndex={index}
        drawings={section.drawings}
        capacity={section.capacity}
        nextDrawing={null}
        ref={this.gridSections[index]}
        smallCount={smallCount}
        mediumCount={mediumCount}
        largeCount={largeCount}
        config={this.props.config}
      />
    })
    this.setState({ sections: gridSections })
  }

  render () {
    const config = this.props.config
    const repeatBg = this.props.config.repeat_bg ? 'repeat' : 'no-repeat'
    const coverBg = this.props.config.repeat_bg ? 'auto' : 'cover'

    return (
      <Drawings
        gridSectionCount={config.grid_section_count}
        backgroundSize={coverBg}
        backgroundRepeat={repeatBg}
        backgroundImage={`url(${this.props.config.bg_image})`}
      >
        {this.state.sections}
      </Drawings>
    )
  }
}

export default Wall
