import React from 'react'
import Store from '../Store.jsx'
import { AdminsIndex } from './dashboard/AdminsIndex'
import { Toaster } from 'react-hot-toast'

const AdminsIndexApp = (props) => (
  <Store>
    <AdminsIndex admin={props.currentAdmin} />
    <Toaster />
  </Store>
)

export default AdminsIndexApp
