import React, { useState, useEffect } from "react";
import { UrlCopyer } from "./UrlCopyer";
import styled from "styled-components";
import { injectStyles } from "../InjectStyles";

const Layout = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  padding: 2em;
  max-width: 900px;
  margin: 0 auto;
`;

const FlexWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const HeaderText = styled.div`
  font-weight: 800;
  font-size: 2em;
  text-align: center;
`;

const ImageWrapper = styled.div`
  margin: 1em 0;
  border: 2px solid black;
`;

const Image = styled.img`
  width: 100%;
`;

const ShareButton = styled.div`
  background: #666666;
  color: #ffffff;
  font-weight: bold;
  font-size: 1.5em;
  padding: 0.5em 1em;
  border-radius: 5px;
`;

const share = () => {
  if (navigator.share) {
    navigator
      .share({
        title: "Share Your Drawing",
        text: "",
        url: window.location.href,
      })
      .then(() => {
        console.log("Shared successfully!");
      })
      .catch((error) => {
        throw new Error("Error sharing:", error);
      });
  } else {
    throw new Error("Web Share API is not supported in your browser.", error);
  }
};

const ShowDrawingApp = ({ drawing, styles }) => {
  const [showingUrlInput, setShowingUrlInput] = useState(false);

  useEffect(() => {
    if (styles) injectStyles(styles);
  }, []);

  const shareUrl = () => {
    console.log("shareUrl");
    try {
      share();
    } catch (err) {
      console.log("failed");
      setShowingUrlInput(true);
    }
  };

  return (
    <Layout className="show-drawing">
      <HeaderText className="header">Share your drawing!</HeaderText>
      <FlexWrapper className="container">
        <ImageWrapper className="image-container">
          <Image className="image" src={drawing.image.url} />
        </ImageWrapper>
        <ShareButton className="share-button" onClick={shareUrl}>
          Share
        </ShareButton>
        {showingUrlInput && <UrlCopyer className="url-copy" url={window.location.href} />}
      </FlexWrapper>
    </Layout>
  );
};
export default ShowDrawingApp;
