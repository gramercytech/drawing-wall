import React from "react";
import TabletAppStore from "./tablet/TabletAppStore";
import Start from "./tablet/Start";
import NewUser from "./tablet/NewUser";
import Terms from "./tablet/Terms";
import { Art } from "./tablet/Art";
import { ThankYou } from "./tablet/ThankYou";
import { injectStyles } from "../InjectStyles";

class Tablet extends React.Component {
  constructor(props) {
    super(props);
    this.pages = { Start, NewUser, Terms, Art, ThankYou };
    this.state = {
      drawing: {},
      page: Start,
      iPad: null,
    };
  }

  componentDidMount() {
    var iPad = window.getUrlParam("ipad", "0");
    this.setState({ iPad });
    injectStyles(this.props.styles);
  }

  setDrawing(drawingParams, submit) {
    const drawing = Object.assign(this.state.drawing, drawingParams);
    this.setState({ drawing }, () => {
      if (submit) this.submitDrawing();
    });
  }

  setPage(pageName) {
    let page = this.pages[pageName];
    this.setState({ page });
  }

  submitDrawing() {
    // if (this.state.drawing.id) this.updateDrawing()
    // else this.createDrawing()
    this.createDrawing();
  }

  createDrawing() {
    let self = this;
    // TODO use api layer
    const url = [
      "/projects/",
      this.props.settings["project_id"],
      "/drawings",
    ].join("");
    window
      .post(url, { drawing: this.state.drawing, ipad: this.state.iPad })
      .then((res) => {
        const drawing = Object.assign(this.state.drawing, {
          id: res.data.id,
          projectId: res.data.project_id,
        });
        self.setState({ drawing });
      })
      .catch((error) => {
        self.setState({ drawing: {} });
        console.error(error);
      });
  }

  updateDrawing() {
    let self = this;
    // TODO use api layer
    window
      .put("/drawings/" + this.state.drawing.id, {
        drawing: this.state.drawing,
      })
      .then((res) => {
        self.setState({ drawing: {} });
      })
      .catch((error) => {
        self.setState({ drawing: {} });
        console.error(error);
      });
  }

  render() {
    let Page = this.state.page;
    // const drawingId = this.state.drawing ? this.state.drawing.id : null;
    return (
      <div id="drawing-content">
        <TabletAppStore>
          <Page
            drawing={this.state.drawing}
            setDrawing={this.setDrawing.bind(this)}
            setPage={this.setPage.bind(this)}
            settings={this.props.settings}
          />
        </TabletAppStore>
      </div>
    );
  }
}

export default Tablet;
