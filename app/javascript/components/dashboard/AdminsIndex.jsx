import React, { useContext, useEffect } from 'react'
import { Context, setAdmins, setAdmin } from '../../Store.jsx'
import { getAdmins, createAdmin } from '../../api/admins'
import { AdminsList } from './AdminsList'
// import { InvitesList } from './InvitesList'
import { NewAdminForm } from './NewAdminForm'
import toast from 'react-hot-toast'
import { ErrorToast } from './ErrorToast'

export const AdminsIndex = ({ admin }) => {
  const { state, dispatch } = useContext(Context)
  const { admins, currentAdmin } = state

  const loadAdmins = () => {
    getAdmins().then((res) => {
      dispatch(setAdmins(res.admins))
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  // const loadInvites = () => {
  //   getAdminInvites().then((res) => {
  //     dispatch(setInvites(res.invites))
  //   }).catch((e) => {
  //     ErrorToast(e)
  //   })
  // }

  useEffect(() => {
    loadAdmins()
    // loadInvites()
    dispatch(setAdmin(admin))
  }, [dispatch, setAdmin])

  const onSubmitNewAdminForm = (email, password) => {
    const admin = { admin: { email: email, password: password, password_confirmation: password }}
    createAdmin(admin).then((res) => {
      toast.success(`Admin ${res.admin.email} was created successfully`)
      loadAdmins()
    }).catch((e) => {
      e.json().then((errors) => {
        ErrorToast(errors)
      })
    })
  }


  return (
    <div>
      <div>
	      <AdminsList  admins={admins} currentAdmin={currentAdmin} />
      </div>
      <hr/>
      <NewAdminForm
        buttonText="New Admin"
        onSubmit={onSubmitNewAdminForm}
      />
    </div>
  )
}
