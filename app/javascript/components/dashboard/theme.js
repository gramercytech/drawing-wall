import styled from 'styled-components'

export const FontSize = {
  veryLarge: '2rem',
  large: '1.5rem',
}

export const Colors = {
  white: '#ffffff',
  gray: '#efefef',
  amethyst: '#9b4dca',
  'sea-green': '#297d4f',
  diesel: '#0e0004',
  punch: '#dd403a'
}

export const Spacing = {
  snug: '.5rem',
  normal: '1rem',
  roomy: '1.5rem'
}

export const Header = styled.div`
  font-weight: bold;
  font-size: ${FontSize.veryLarge};
  padding: ${Spacing.roomy} ${Spacing.snug};
`

export const Header2 = styled(Header)`
  font-weight: bold;
  font-size: ${FontSize.large};
  padding: ${Spacing.roomy} ${Spacing.snug};
`

export const Grid = styled.div`
  display: grid;
`

export const GridRow = styled.div`
  display: grid;
  padding: ${Spacing.normal};
  grid-template-columns: ${(props) => '1fr '.repeat(props.columns - 1)} auto;
  &:nth-child(even) {
    background: ${Colors.gray};
  }
`

export const GridColumn = styled.div`
  display: grid;
`

export const FlexColumn = styled.div`
  display: flex;
`

export const TextRight = styled.div`
  text-align: right;
`

export const Button = styled.button`
  padding: ${Spacing.snug} ${Spacing.roomy};
  color: #ffffff;
  background-color: ${(props) => props.mode === 'danger' ? Colors.punch : Colors.amethyst};
  border: none;
  border-radius: ${Spacing.snug};
  font-weight: bold;
  line-height: unset;
  text-transform: unset;
`
