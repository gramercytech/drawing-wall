import React, { useState } from "react";
import { Button, TextRight, Header2 } from "./theme";

export const EditProjectForm = ({ onSubmit, project }) => {
  const [name, setName] = useState(project.name);
  const [config, setConfig] = useState(JSON.stringify(project.config));
  const [wallStyles, setWallStyles] = useState(project.wall_styles);
  const [tabletStyles, setTabletStyles] = useState(project.tablet_styles);
  const [showDrawingStyles, setShowDrawingStyles] = useState(
    project.show_drawing_styles
  );

  const handleSubmit = (
    event,
    { name, config, wallStyles, tabletStyles, showDrawingStyles }
  ) => {
    event.preventDefault();
    onSubmit({
      name,
      config,
      wall_styles: wallStyles,
      tablet_styles: tabletStyles,
      show_drawing_styles: showDrawingStyles,
    });
  };

  return (
    <form>
      <Header2>Name</Header2>
      <input
        type="text"
        onChange={(e) => setName(e.target.value)}
        value={name}
      />

      <Header2>Config</Header2>
      <textarea onChange={(e) => setConfig(e.target.value)} value={config} />

      <Header2>Wall Styles</Header2>
      <textarea
        onChange={(e) => setWallStyles(e.target.value)}
        value={wallStyles}
      />

      <Header2>iPad Styles</Header2>
      <textarea
        onChange={(e) => setTabletStyles(e.target.value)}
        value={tabletStyles}
      />

      <Header2>Share Page Styles</Header2>
      <textarea
        onChange={(e) => setShowDrawingStyles(e.target.value)}
        value={showDrawingStyles}
      />

      <TextRight>
        <Button
          onClick={(e) =>
            handleSubmit(e, {
              name,
              config,
              wallStyles,
              tabletStyles,
              showDrawingStyles,
            })
          }
        >
          Save
        </Button>
      </TextRight>
    </form>
  );
};
