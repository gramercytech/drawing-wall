import React, { useContext, useEffect, useState, useMemo } from 'react'
import toast from 'react-hot-toast'
import Wall from '../Wall'
import { Context, setAdmin, setCurrentProject, setDrawings } from '../../Store'
import { Urls } from '../../api/urls'
import { getProject, updateProject, deleteProject } from '../../api/projects'
import { getDrawings, updateDrawing } from '../../api/drawings'
import { Header } from './theme'
import { Settings } from './Settings'
import { Drawings } from './Drawings'
import { Collaborators } from './Collaborators'
import { TabbedInterface } from './TabbedInterface'
import { ErrorToast } from './ErrorToast'

const projectsUrl = `${Urls().base}/projects`

export const EditProject = ({ admin, projectId }) => {
  const { state, dispatch } = useContext(Context)
  const { currentProject, drawings } = state
  // Wall isn't so good at rendering when drawings prop changes
  // so don't render until we're done fetching
  const [drawingsFetched, setDrawingsFetched] = useState(false)

  const loadDrawings = () => {
    getDrawings(projectId).then((res) => {
      dispatch(setDrawings(res.drawings))
      setDrawingsFetched(true)
    }).catch((e) => {
      console.error(e)
    })
  }

  useEffect(() => {
    dispatch(setAdmin(admin))
  }, [dispatch, setAdmin])


  useEffect(() => {
    getProject(projectId).then((res) => {
      dispatch(setCurrentProject(res.project))
      loadDrawings()
    }).catch((e) => {
      ErrorToast(e)
    })
  }, [dispatch, getProject, setCurrentProject, toast])

  const saveProject = (project) => {
    updateProject({ id: projectId, ...project }).then((res) => {
      toast.success(`${res.project.name} was updated successfully`)
      dispatch(setCurrentProject(res.project))
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const onClickDeleteProject = (project) => {
    deleteProject({ ...project, id: projectId }).then((res) => {
      window.location = projectsUrl
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const toggleDrawingHidden = (e, drawing) => {
    const hidden = e.target.checked
    updateDrawing({ ...drawing, hidden }).then((res) => {
      toast.success(`Drawing was ${hidden ? 'hidden' : 'unhidden'}`)
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const toggleIsTestDrawing = (e, drawing) => {
    const test = e.target.checked
    updateDrawing({ ...drawing, test }).then((res) => {
      toast.success(`Drawing set to ${test ? 'test' : 'live'} mode`)
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const tabs = useMemo(() => [
    {
      title: 'Settings',
      content: (
        <Settings
          onSubmit={saveProject}
          onClickDelete={onClickDeleteProject}
          project={currentProject}
          admin={admin}
        />
      )
    },
    {
      title: 'Drawings',
      content: (
        <Drawings
          drawings={drawings}
          onHiddenChanged={toggleDrawingHidden}
          onTestChanged={toggleIsTestDrawing}
          exportPath={`${projectsUrl}/${projectId}/exports`}
        />
      )
    },
    { title: 'Collaborators', content: ( <Collaborators />) }
  ], [currentProject, drawings])

  return (
    <div>
      {currentProject && (
        <div>
          <Header>{currentProject.name}</Header>
          {drawingsFetched && <a href={`${projectsUrl}/${projectId}`}>
            <Wall config={currentProject.config} drawings={drawings} />
          </a>}
          <TabbedInterface tabs={tabs} />
        </div>
      )}
    </div>
  )
}
