import React from 'react'
import { EditProjectForm } from './EditProjectForm'
import { Urls } from '../../api/urls'
import { Header, Button, TextRight } from './theme'
import range from 'range'
import { HorizontalList } from './HorizontalList'

export const Settings = ({ admin, project, onSubmit, onClickDelete }) => {
  const newDrawingUrl = `${Urls().base}/projects/${project.id}/drawings/new`
  const iPadCount = project.config.grid_section_count

  const iPads = range.range(0, iPadCount).map((index) => (
    <a href={`${newDrawingUrl}?ipad=${index}`}>iPad {index}</a>
  ))

  return (
    <div>
      <EditProjectForm onSubmit={onSubmit} project={project} />
      {project.created_by.id === admin.id && (
        <TextRight>
          <Button mode="danger" onClick={() => onClickDelete()}>Delete Project</Button>
        </TextRight>
      )}
      <Header>iPads</Header>
      <HorizontalList items={iPads} />
    </div>
  )
}
