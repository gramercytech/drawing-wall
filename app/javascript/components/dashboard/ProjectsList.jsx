import React from 'react'
import styled from 'styled-components'
import { Spacing, Colors, Grid, GridRow, Header } from './theme'

const LinkSeparator = () => (<span> / </span>)

const OwnerLabelWrapper = styled.span`
  padding: 0 ${Spacing.roomy};

  span {
    color: ${Colors['sea-green']};
  }
`
const OwnerLabel = () => (
  <OwnerLabelWrapper>
    (<span>Owner</span>)
  </OwnerLabelWrapper>
)

export const ProjectsList = ({ projects, admin }) => {
  const projectsPath = `/projects`
  const projectRows = projects.map((project) => (
    <GridRow key={project.id} columns={2}>
      <div>
        {project.name}
        {project.created_by.id === admin.id && <OwnerLabel />}
      </div>
      <div>
          <a href={`${projectsPath}/${project.id}`}>Wall</a>
          <LinkSeparator />
          <a href={`${projectsPath}/${project.id}/drawings/new`}>Draw</a>
          <LinkSeparator />
          <a href={`${projectsPath}/${project.id}/edit`}>Admin</a>
      </div>
    </GridRow>
  ))

  return (
    <div>
      {projects.length > 0 && <Header>Projects</Header>}
      <Grid>
        {projectRows}
      </Grid>
    </div>
  )
}
