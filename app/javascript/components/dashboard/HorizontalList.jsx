import React from 'react'
import styled from 'styled-components'
import { Spacing } from './theme'

const StyledUl = styled.ul`
  list-style-type: none;
  display: grid;
  grid-template-columns: repeat(${(props) => props.columns}, auto) 1fr;
  margin: 0;
`

const StyledLi = styled.li`
  padding: ${Spacing.normal};
  cursor: pointer;
`

export const HorizontalList = ({ items }) => {
  const styledItems = items.map((item, index) => (<StyledLi key={index}>{item}</StyledLi>))
  return (<StyledUl columns={items.length}>{styledItems}</StyledUl>)
}
