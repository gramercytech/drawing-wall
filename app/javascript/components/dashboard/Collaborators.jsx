import React, { useEffect, useContext } from 'react'
import { SingleInputForm } from './SingleInputForm'
import {
  Context, setCollaborators, setProjectInvites
} from '../../Store'
import { getCollaborators, removeCollaboration } from '../../api/collaborators'
import {
  getProjectInvites,
  inviteToProject,
  cancelProjectInvite
} from '../../api/projectInvites'
import toast from 'react-hot-toast'
import { ErrorToast } from './ErrorToast'
import { Grid, GridRow, Header, FlexColumn } from './theme'

export const Collaborators = ({
}) => {
  const { state, dispatch } = useContext(Context)

  const { currentAdmin, currentProject, collaborators, projectInvites } = state
  const projectId = currentProject.id
  const currentAdminId = currentAdmin ? currentAdmin.id : null
  const projectCreatorId = currentProject ? currentProject.created_by.id : null

  const loadPendingInvites = () => {
    getProjectInvites(projectId).then((res) => {
      dispatch(setProjectInvites(res))
    }).catch((e) => {
      console.error(e)
    })
  }

  const loadCollaborators = () => {
    getCollaborators(projectId).then((res) => {
      dispatch(setCollaborators(res))
    }).catch((e) => {
      console.error(e)
    })
  }

  const cancel = (e, invite) => {
    e.preventDefault()
    cancelProjectInvite({ projectId, ...invite }).then((res) => {
      loadPendingInvites()
      toast.success(`${invite.admin.email}'s invitation to ${currentProject.name} was canceled`)
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const remove = (e, collaborator) => {
    e.preventDefault()
    removeCollaboration({ projectId, ...collaborator }).then((res) => {
      if (currentAdminId === collaborator.admin.id) {
        window.location = window.location.origin
      } else {
        loadCollaborators()
        toast.success(`${collaborator.admin.email} was removed from ${currentProject.name}`)
      }
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const onSubmitCreateInvite = (email) => {
    inviteToProject({ email, projectId }).then((res) => {
      loadPendingInvites()
      toast.success(`${email} was invited to ${currentProject.name}`)
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  useEffect(() => {
    loadPendingInvites()
    loadCollaborators()
  }, [dispatch, toast])

  return (
    <div>
      <Header>Created By</Header>
      <Grid>
        <GridRow columns={1}>
          <div>Created by {currentProject && currentProject.created_by.email}</div>
        </GridRow>
      </Grid>

      <Header>Collaborators</Header>
      <Grid>
        {collaborators.map((collab) => (
          <GridRow key={collab.id} columns={2}>
            <div>{collab.admin.email}</div>
            <FlexColumn>
              {(projectCreatorId === currentAdminId || collab.admin.id === currentAdminId) &&
                <a href="" onClick={(e) => remove(e, collab)}>Remove</a>
              }
            </FlexColumn>
          </GridRow>
        ))}
      </Grid>

      <Header>Pending Invites</Header>
      <Grid>
        {projectInvites.map((invite) => (
          <GridRow key={invite.id} columns={2}>
            <div>{invite.admin.email}</div>
            <FlexColumn>
              {projectCreatorId === currentAdminId &&
                <a href="" onClick={(e) => cancel(e, invite)}>Cancel</a>
              }
            </FlexColumn>
          </GridRow>
        ))}
      </Grid>

      {projectCreatorId === currentAdminId && (
        <SingleInputForm
          placeholder="Collaborator email"
          buttonText="Invite Collaborator"
          onSubmit={onSubmitCreateInvite}
        />
      )}
    </div>
  )
}

