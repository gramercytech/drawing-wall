import React, { useState } from 'react'
import { Spacing, Grid, GridRow, Button } from './theme'
import styled from 'styled-components'

const StyledForm = styled.form`
  margin-top: ${Spacing.roomy};

  input {
    margin: 0;
    border-radius: ${Spacing.snug} 0 0 ${Spacing.snug};
  }
`

const StyledButton = styled(Button)`
  border-radius: 0 ${Spacing.snug} ${Spacing.snug} 0;
`

export const NewAdminForm = ({buttonText, onSubmit }) => {
  const [inputVal, setInputVal] = useState('')
  const [inputEmailVal, setInputEmailVal] = useState('')
  const [inputPasswordVal, setInputPasswordVal] = useState('')
  const [inputPasswordConfirmationVal, setInputPasswordConfirmationVal] = useState('')

  const handleSubmit = (event) => {
    event.preventDefault()
    if (inputPasswordVal !== inputPasswordConfirmationVal) {
      alert('Passwords do not match');
      return; 
    } else {
      onSubmit(inputEmailVal, inputPasswordVal)
    }
  }

  return (
    <StyledForm>
        <h4>New Admin</h4>
        <input
        	name="email"
          type="text"
          placeholder="email"
          autoComplete="off"
          onChange={(e) => setInputEmailVal(e.target.value)}
        />
        <input
        	name="password"
          type="password"
          placeholder="password"
          autoComplete="off"
          onChange={(e) => setInputPasswordVal(e.target.value)}
        />
        <input
        	name="password_confirmation"
          type="password"
          placeholder="password confirm"
          autoComplete="off"
          onChange={(e) => setInputPasswordConfirmationVal(e.target.value)}
        />
    
        <StyledButton onClick={(e) => handleSubmit(e)}>{ buttonText }</StyledButton>
      
    </StyledForm>
  )
}
