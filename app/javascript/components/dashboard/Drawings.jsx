import React from 'react'
import DataTable from 'react-data-table-component'
import styled from 'styled-components'
import { TextRight, Button } from './theme'

const DataTableWrapper = styled.div`
  .rdt_Table {
    display: block;
  }

  .rdt_TableCell {
    div {
      white-space: unset;
    }
  }
`

export const Drawings = ({ drawings, exportPath, onHiddenChanged, onTestChanged }) => {
  const columns = [
    { name: 'Id', selector: (drawing) => drawing.id, grow: 0 },
    {
      name: 'Created',
      selector: (drawing) => (new Date(drawing.created_at)).toLocaleString()
    },
    {
      name: 'Url',
      selector: (drawing) => drawing.image.url.match(/[^?]*/)[0],
      grow: 2
    },
    {
      name: 'Image',
      selector: (drawing) => <img src={`${drawing.image.url}`} />
    },
    {
      name: 'Hidden',
      selector: (drawing) => (
        <input
          type="checkbox"
          defaultChecked={drawing.hidden}
          onChange={(e) => onHiddenChanged(e, drawing)}
        />
      ),
      grow: 0
    },
    {
      name: 'Test',
      selector: (drawing) => (
        <input
          type="checkbox"
          defaultChecked={drawing.test}
          onChange={(e) => onTestChanged(e, drawing)}
        />
      ),
      grow: 0
    }
  ];

  const token = document.querySelector(
    'meta[name="csrf-token"]'
  ).getAttribute('content')

  return (
    <div>
      <DataTableWrapper>
        <DataTable
          pagination
          dense
          fixedHeader
          striped
          columns={columns}
          data={drawings}
          customStyles={{ tableWrapper: { style: 'block'} }}
        />
      </DataTableWrapper>
      <form action={exportPath} method='post'>
        <input type="hidden" name="authenticity_token" value={token} />
        <TextRight>
          <Button>Export Drawings</Button>
        </TextRight>
      </form>
    </div>
  )
}

