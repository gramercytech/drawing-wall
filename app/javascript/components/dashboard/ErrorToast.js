import toast from 'react-hot-toast'

export const ErrorToast = (e) => {
  const message = Object.keys(e).reduce(function (accum, attr) {
    if (accum) accum += ', '
    return `${accum} ${attr} ${e[attr]}`
  }, '')
  return toast.error(message)
}
