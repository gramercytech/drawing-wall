import React from 'react'
import styled from 'styled-components'
import {
  Grid,
  GridRow,
  FlexColumn,
  Header
} from './theme'

const Spacer = styled.div`
  width: 1em;
`

export const InvitesList = ({ invites, onAccept, onReject }) => {
  const accept = (event, invite) => {
    event.preventDefault()
    onAccept(invite)
  }

  const reject = (event, invite) => {
    event.preventDefault()
    onReject(invite)
  }

  const inviteRows = invites.map((invite) => (
    <GridRow key={invite.id} columns={2}>
      <div>
        {invite.project.name}
      </div>
      <FlexColumn>
        <a href="" onClick={(e) => accept(e, invite)}>Accept</a>
        <Spacer />
        <a href="" onClick={(e) => reject(e, invite)}>Reject</a>
      </FlexColumn>
    </GridRow>
  ))

  return (
    <div>
      {invites.length > 0 && <Header>Pending Invites</Header>}
      <Grid>
        {inviteRows}
      </Grid>
    </div>
  )
}
