import React, { useState } from 'react'
import { Spacing, Grid, GridRow, Button } from './theme'
import styled from 'styled-components'

const StyledForm = styled.form`
  margin-top: ${Spacing.roomy};

  input {
    margin: 0;
    border-radius: ${Spacing.snug} 0 0 ${Spacing.snug};
  }
`

const StyledButton = styled(Button)`
  border-radius: 0 ${Spacing.snug} ${Spacing.snug} 0;
`

export const SingleInputForm = ({ placeholder, buttonText, onSubmit }) => {
  const [inputVal, setInputVal] = useState('')

  const handleSubmit = (event, params) => {
    event.preventDefault()
    onSubmit(params)
  }

  return (
    <StyledForm>
      <Grid>
        <GridRow columns={2}>
          <input
            type="text"
            placeholder={placeholder}
            onChange={(e) => setInputVal(e.target.value)}
          />
          <StyledButton onClick={(e) => handleSubmit(e, inputVal)}>{ buttonText }</StyledButton>
        </GridRow>
      </Grid>
    </StyledForm>
  )
}
