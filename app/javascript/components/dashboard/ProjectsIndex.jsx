import React, { useContext, useEffect } from 'react'
import { Context, setProjects, setInvites, setAdmin } from '../../Store.jsx'
import { getProjects, createProject } from '../../api/projects'
import { getAdminInvites, acceptAdminInvite, rejectAdminInvite } from '../../api/adminInvites'
import { ProjectsList } from './ProjectsList'
import { InvitesList } from './InvitesList'
import { SingleInputForm } from './SingleInputForm'
import toast from 'react-hot-toast'
import { ErrorToast } from './ErrorToast'

export const ProjectsIndex = ({ admin }) => {
  const { state, dispatch } = useContext(Context)
  const { projects, invites, currentAdmin } = state

  const loadProjects = () => {
    getProjects().then((res) => {
      dispatch(setProjects(res.projects))
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const loadInvites = () => {
    getAdminInvites().then((res) => {
      dispatch(setInvites(res.invites))
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  useEffect(() => {
    loadProjects()
    loadInvites()
    dispatch(setAdmin(admin))
  }, [dispatch, setAdmin])

  const onClickAccept = (invite) => {
    acceptAdminInvite(invite.id).then((res) => {
      loadProjects()
      loadInvites()
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const onClickReject = (invite) => {
    rejectAdminInvite(invite.id).then((res) => {
      loadInvites()
    }).catch((e) => {
      ErrorToast(e)
    })
  }

  const onSubmitNewProjectForm = (name) => {
    createProject({ name }).then((res) => {
      toast.success(`${res.project.name} was created successfully`)
      loadProjects()
    }).catch((e) => {
      console.log('noep')
      console.log(e)
      e.json().then((errors) => {
        ErrorToast(errors)
      })
    })
  }

  return (
    <div>
      <InvitesList invites={invites} onAccept={onClickAccept} onReject={onClickReject} />
      <ProjectsList  projects={projects} admin={currentAdmin} />
      <SingleInputForm
        placeholder="New Project Name"
        buttonText="New Project"
        onSubmit={onSubmitNewProjectForm}
      />
    </div>
  )
}
