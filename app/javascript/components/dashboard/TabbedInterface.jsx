import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Colors } from './theme'
import { HorizontalList } from './HorizontalList'

const TabHeader = styled.span`
  color: ${(props) => props.active ? Colors.amethyst : Colors.diesel};
  ${(props) => props.active ? 'font-weight: bold;' : null}
`

export const TabbedInterface = ({ tabs }) => {
  const [activeTab, setActiveTab] = useState(null)
  const tabHeaders = tabs.map((tab) => (
    <TabHeader onClick={() => setActiveTab(tab)} active={activeTab && activeTab.title === tab.title}>
      {tab.title}
    </TabHeader>
  ))

  useEffect(() => {
    if (tabs.length) setActiveTab(tabs[0])
  }, [tabs])

  return (
    <div>
      <HorizontalList items={tabHeaders} />
      {activeTab && activeTab.content}
    </div>
  )
}

