import React from 'react'
import styled from 'styled-components'
import { Spacing, Colors, Grid, GridRow, Header } from './theme'

const LinkSeparator = () => (<span> | </span>)

export const AdminsList = ({ admins, currentAdmin }) => {
  const adminsPath = `/admins`
  const adminRows = admins.map((admin) => (
    <GridRow key={admin.id} columns={2}>
      <div>
        {admin.email}
      </div>
      <div>
        <a href={`${adminsPath}/${admin.id}/edit`}>Edit</a>
      </div>
    </GridRow>
  ))

  return (
    <div>
      {admins.length > 0 && <Header>Admins</Header>}
      <Grid>
        {adminRows}
      </Grid>
    </div>
  )
}
