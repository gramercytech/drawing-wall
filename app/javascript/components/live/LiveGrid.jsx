import React from 'react'
import LiveGridSection from './LiveGridSection'

class LiveGrid extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      sections: []
    }
  }

  componentDidMount () {
    window.App.messages = window.App.cable.subscriptions.create('DrawingsChannel', {
      received: (data) => {
        if (data.type === 'update') {
          let liveDrawings = document.getElementsByClassName('live-drawing-canvas')
          for (let canvas of liveDrawings) {
            console.log(canvas)
            let ctx = canvas.getContext('2d')
            let image = new window.Image()
            image.onload = () => {
              ctx.drawImage(image, 0, 0)
            }
            image.src = data.drawing
          }
        } else if (data.type === 'complete') {
          // do nothing for now
        }
      }
    })
    this.createGridSections()
  }

  createGridSections () {
    let gridSections = [1, 2, 3, 4, 5, 6].map((num) => {
      return <LiveGridSection key={num} />
    })
    this.setState({ sections: gridSections })
  }

  render () {
    return (
      <div id='drawings' className='live-grid'>
        {this.state.sections}
      </div>
    )
  }
}

export default LiveGrid
