import React from 'react'

class LiveGridSection extends React.Component {
  render () {
    return (
      <div className='grid-section live'>
        <canvas className='live-drawing-canvas' width='1500' height='1000' />
        <canvas className='live-drawing-canvas' width='1500' height='1000' />
        <canvas className='live-drawing-canvas' width='1500' height='1000' />
        <canvas className='live-drawing-canvas' width='1500' height='1000' />
        <canvas className='live-drawing-canvas' width='1500' height='1000' />
        <canvas className='live-drawing-canvas' width='1500' height='1000' />
      </div>
    )
  }
}

export default LiveGridSection
