import React from 'react'
import Store from '../Store.jsx'
import { EditProject } from './dashboard/EditProject'
import { Toaster } from 'react-hot-toast'

const EditProjectApp = ({ currentAdmin, projectId }) => (
  <Store>
    <EditProject admin={currentAdmin} projectId={projectId} />
    <Toaster />
  </Store>
)

export default EditProjectApp
