import React from 'react'
import ReactDOM from 'react-dom'
import GridSection from './GridSection'

class LatestDrawing extends GridSection {
  constructor (props) {
    super(props)
    this.img = React.createRef()
    this.featureDuration = 20
    this.maxActiveTime = 120
    this.state = {
      drawings: [],
      maxDrawings: 2,
      queue: []
    }
  }

  addToQueue (drawing) {
    drawing.time = Math.floor(new Date().getTime() / 1000)
    const queue = this.state.queue.concat(drawing)
    this.setState({ queue })
  }

  componentDidMount () {
    super.componentDidMount()
    let self = this
    setInterval(() => {
      const queueLength = self.state.queue.length
      if (queueLength > 0) {
        const image = document.querySelector('#latest-drawing-image img')
        const timeMounted = Number(image.dataset.time)
        const currentTime = Math.floor(new Date().getTime() / 1000)
        const allotment = queueLength > 1 ? self.featureDuration : self.maxActiveTime
        if (currentTime - timeMounted > allotment) {
          self.dequeue()
        }
      }
    }, 2010)
  }

  dequeue () {
    let queue = this.state.queue
    const plucked = queue.shift()
    this.props.replaceOldestWith(plucked)
    document.querySelector('#latest-drawing-image img').classList.add('hidden')
    let self = this
    setTimeout(() => {
      self.setState({ queue })
    }, 4000)
  }

  fadeIn () {
    window.fadein(ReactDOM.findDOMNode(this.img.current))
  }

  render () {
    return (
      <div id='latest-drawing' className='grid-section'>
        {this.state.drawings}
        <div id='latest-drawing-image'>
          {this.state.queue.length > 0 &&
            <img
              src={this.state.queue[0].src}
              data-time={this.state.queue[0].time}
              className='hidden'
              ref={this.img}
              onLoad={() => this.fadeIn()}
            />
          }
          {this.state.queue.length === 0 &&
            <h1></h1>
          }
        </div>
      </div>
    )
  }
}

export default LatestDrawing
