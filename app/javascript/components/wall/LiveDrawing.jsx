import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'

const Live = styled.div`
  justify-content: center;
  align-items: center;
  z-index: 1;

  grid-column: ${props => props.placementX} / span ${props => props.columns};
  grid-row: ${props => props.placementY} / span ${props => props.rows};
`



class LiveDrawing extends React.Component {
  constructor (props) {
    super(props)
    this.canvas = React.createRef()
    this.state = {
      drawings: [],
      maxDrawings: 2,
      idle: true
    }
    this.idleImg = React.createRef()
  }

  update (data) {
    if (data && data.drawing) this.updateLiveDrawing(data.drawing)
    else this.goIdle()
  }

  updateLiveDrawing (drawing) {
    let self = this
    this.setState({ idle: false }, () => {
      var canvas = this.canvas.current
      var ctx = canvas.getContext('2d')
      var image = new window.Image()
      const canvasWidth = self.props.canvasWidth
      const canvasHeight = self.props.canvasHeight
      image.onload = () => {
        ctx.clearRect(0, 0, canvasWidth, canvasHeight)
        ctx.drawImage(image, 0, 0)
      }
      image.src = drawing
    })
  }

  goIdle () {
    this.setState({ idle: true })
  }

  fadeOut () {
    window.fadeout(ReactDOM.findDOMNode(this.idleImg.current))
  }

  fadeIn () {
    window.fadein(ReactDOM.findDOMNode(this.idleImg.current))
  }

  render () {
    const config = this.props.config
    return (
      <Live
        placementX={config.live_placement_x}
        placementY={config.live_placement_y}
        rows={config.live_rows}
        columns={config.live_columns}
      >
        {this.state.drawings}
        <div id='live-drawing-image' className='idle-live'>
          {!this.state.idle && <canvas className='live-drawing-canvas' width={this.props.canvasWidth} height={this.props.canvasHeight} ref={this.canvas} />}
          {this.state.idle && <img src={this.props.idleImage} ref={this.idleImg} onLoad={() => this.fadeIn()} />}
        </div>
      </Live>
    )
  }
}

export default LiveDrawing
