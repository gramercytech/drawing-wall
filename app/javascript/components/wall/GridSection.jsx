import React from 'react'
import LiveDrawing from './LiveDrawing'
import SectionDrawing from './SectionDrawing'
var shuffle = require('shuffle-array')
import styled from 'styled-components'

const StyledGridSection = styled.div`
  display: grid;
  grid-template-columns: repeat(${props => props.gridSectionColumns}, 1fr);
  grid-template-rows: repeat(${props => props.gridSectionRows}, 1fr);
  grid-auto-flow: dense;
`

class GridSection extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      drawings: [],
      maxDrawings: props.capacity
    }
    this.drawingSections = []
    this.liveDrawing = React.createRef()
  }

  removeDrawingById (id) {
    // this.drawingSections.forEach((drawing) => {
    //   drawing.current.removeDrawingById(id)
    // })
    if (this.props.dupImages === '1') {
      this.state.drawings.forEach((drawing) => {
        // drawing.current.removeDrawingById(id)
        drawing.ref.current.removeDrawingById(id)
      })
    } else {
      this.drawingSections.forEach((drawing) => {
        drawing.current.removeDrawingById(id)
      })
    }
  }

  showDrawingById (id) {
    this.drawingSections.forEach((drawing) => {
      drawing.current.showDrawingById(id)
    })
  }

  updateLiveDrawing (data) {
    this.liveDrawing.current.update(data)
  }

  weightedRand2 (spec) {
    var i = 0
    var sum = 0
    var r = Math.random()
    for (i in spec) {
      sum += spec[i]
      if (r <= sum) return i
    }
  }

  fillArray (value, len) {
    var arr = []
    for (var i = 0; i < len; i++) {
      arr.push(value)
    }
    return arr
  }

  componentDidMount () {
    let sizeAllocations = this.fillArray('small', this.props.smallCount).concat(
      this.fillArray('medium', this.props.mediumCount)
    ).concat(
      this.fillArray('large', this.props.largeCount)
    )
    // TODO figure out this thing with the different image sizes
    // Start with the medium?
    // let sizeAllocations = this.fillArray('small', this.props.smallCount);
    shuffle(sizeAllocations)
    let self = this
    let drawings = []
    if (this.props.dupImages === '1') {
      while (drawings.length < self.state.maxDrawings) { // self.state.maxDrawings + liveSpace
        // TODO fill drawings under the live space with no-access divs
        // if (inLiveSpace(drawings.length) { drawings.push(<SectionDrawing noaccess={true}/>) })
        // or is that really the best way to do it? how will it handle
        // distribution of new drawings?
        var randomIndex = Math.floor(Math.random() * self.props.drawings.length)
        var randomDrawing = self.props.drawings[randomIndex]
        var drawingSize = sizeAllocations.pop()
        self.drawingSections.push(React.createRef())
        drawings.push(
          <SectionDrawing
            key={drawings.length}
            id={randomDrawing.id}
            image={randomDrawing.image.url}
            drawingSize={drawingSize}
            ref={self.drawingSections[self.drawingSections.length - 1]}
          />
        )
      }
    } else {
      drawings = this.props.drawings.sort((a, b) => a.id > b.id);
      const overcappedBy = drawings.length - this.state.maxDrawings;
      if (overcappedBy > 0) {
        drawings = drawings.slice(overcappedBy);
      }
      drawings = drawings.map((drawing, index) => {
        this.drawingSections.push(React.createRef())
        var drawingSize = sizeAllocations.pop()
        return <SectionDrawing key={index} drawingSize={drawingSize} image={drawing.image} id={drawing.id} ref={this.drawingSections[index]} />
      })
    }
    shuffle(drawings)
    this.setState({ drawings })
  }

  render () {
    const canvasHeight = 1000
    const canvasWidth = canvasHeight / this.props.config.ipad.canvas_ratio

    return (
      <StyledGridSection
        id={'grid-section-' + this.props.sectionIndex}
        data-section-index={this.props.sectionIndex}
        gridSectionRows={this.props.config.grid_section_rows}
        gridSectionColumns={this.props.config.grid_section_columns}
      >
        <LiveDrawing
          ref={this.liveDrawing}
          key={this.props.sectionIndex}
          drawings={this.state.drawings}
          sectionIndex={this.props.sectionIndex}
          idleImage={this.props.config.idle_image}
          canvasWidth={canvasWidth}
          canvasHeight={canvasHeight}
          config={this.props.config}
        />
        {this.state.drawings}
      </StyledGridSection>
    )
  }
}

export default GridSection
