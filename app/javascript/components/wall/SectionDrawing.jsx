import React from 'react'
import ReactDOM from 'react-dom'
class SectionDrawing extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      drawingSize: 'small'
    }
    this.img = React.createRef()
  }

  componentDidMount () {
    var size = this.props.drawingSize ? this.props.drawingSize : this.state.drawingSize
    this.setState({ drawingSize: size })
  }

  removeDrawingById (id) {
    var self = this
    if (id == this.props.id) self.fadeOut();
  }

  showDrawingById (id) {
    var self = this
    if (id == this.props.id) self.fadeIn();
  }

  fadeOut () {
    window.fadeout(ReactDOM.findDOMNode(this.img.current))
  }

  fadeIn () {
    window.fadein(ReactDOM.findDOMNode(this.img.current))
  }

  render () {
    return (
      <div className={`drawing ${this.state.drawingSize}`} data-id={this.props.id}>
        {this.props.image &&
          <img src={this.props.image}
            data-id={this.props.id}
            className='hidden'
            ref={this.img}
            onLoad={() => this.fadeIn()}
          />
        }
      </div>
    )
  }
}

export default SectionDrawing
