var shuffle = require('shuffle-array')
const unglow = window.unglow

module.exports = function () {
  const queue = getInitialQueue()

  return {
    queue: queue,

    rotateOut: function (newMember) {
      const container = queue[0]
      const nextQueue = queue[0]
      if (container.children.length) {
        // Do we need to add hidden or can we just container.children[0].remove() ?
        // container.children[0].classList.add('hidden')
        const child = container.children[0]
        child.classList.add('hidden')
        dropInExistingImage(nextQueue, child)
        setTimeout(function () { dropInNewImage(container, newMember) }, 2000)
      } else {
        dropInNewImage(container, newMember)
      }
      queue.push(queue.shift())
    }
  }

  function dropInNewImage (container, newMember) {
    container.dataset.id = newMember.id
    container.innerHTML = `<img src=${newMember.src} data-id=${newMember.id} class='hidden' onload='fadein(this)' />`
    container.classList.add('glowing')
    unglow(container)
  }

  function dropInExistingImage (container, existingMember) {
    container.dataset.id = existingMember.dataset.id
    container.innerHTML = `<img src=${existingMember.src} data-id=${existingMember.dataset.id} class='hidden' onload='fadein(this)' />`
    container.classList.add('glowing')
    unglow(container)
  }

  function getInitialQueue () {
    // const queue = {}
    // const drawingSections = [].slice.call(document.querySelectorAll('.grid-section'))
    // drawingSections.forEach(function(element) {
    //   const drawingContainers = [].slice.call(document.getElementById(element.id).getElementsByClassName("drawing"))
    //   const categorizedContainers = drawingContainers.reduce(function (accum, container) {
    //     const category = container.dataset.id > 0 ? 'used' : 'unused'
    //     accum[category].push(container)
    //     return accum
    //   }, {used: [], unused: []})
    //   categorizedContainers.used.sort((a, b) => {
    //     return a.dataset.id - b.dataset.id
    //   })
    //   shuffle(categorizedContainers.unused)
    //   var sectionIndex = element.dataset.sectionIndex
    //   queue[sectionIndex.toString()] = categorizedContainers.unused.concat(categorizedContainers.used)
    // });
    // return queue

    // const drawingContainers = [].slice.call(document.querySelectorAll('.drawing'))
    const drawingContainers = [].slice.call(document.querySelectorAll('.drawing.small'))
    const categorizedContainers = drawingContainers.reduce(function (accum, container) {
      const category = container.dataset.id > 0 ? 'used' : 'unused'
      accum[category].push(container)
      return accum
    }, { used: [], unused: [] })

    categorizedContainers.used.sort((a, b) => {
      return a.dataset.id - b.dataset.id
    })
    shuffle(categorizedContainers.unused)
    return categorizedContainers.unused.concat(categorizedContainers.used)
  }
}
