export const injectStyles = (styles) => {
  const body = document.querySelector('body')
  const styleTag = document.createElement('style')
  styleTag.innerText = styles
  body.appendChild(styleTag)
}

