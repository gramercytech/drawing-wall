class DrawingsChannel < ApplicationCable::Channel  
  def subscribed
    stream_from 'drawings'
  end

  def receive(data)
    ActionCable.server.broadcast "drawings", data
  end

  def unsubscribed
  end
end  
