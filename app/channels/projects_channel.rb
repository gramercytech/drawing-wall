class ProjectsChannel < ApplicationCable::Channel  
  def subscribed
    stream_from "project_#{params[:project_id]}"
  end

  def receive(data)
    ActionCable.server.broadcast "project_#{params[:project_id]}", data
  end

  def unsubscribed
  end
end  
