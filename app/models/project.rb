class Project < ApplicationRecord
  belongs_to :created_by, foreign_key: :created_by, class_name: 'Admin', required: true
  has_many :project_invites
  has_many :admin_projects
  has_many :drawings
  validates :name, presence: true

  after_create :set_config_to_default
  before_update :format_config
  before_destroy :delete_drawings
  before_destroy :delete_invites
  before_destroy :delete_collaborations

  def invites
    project_invites
  end

  def collaborations
    admin_projects
  end

  private

  def set_config_to_default
    update(config: Rails.application.config_for(:drawings))
  end

  def format_config
    update(config: JSON.parse(config)) if config.class == String
  end

  def delete_drawings
    drawings.all.each { |d| d.destroy }
  end

  def delete_invites
    invites.all.each { |i| i.destroy }
  end

  def delete_collaborations
    collaborations.all.each { |c| c.destroy }
  end
end
