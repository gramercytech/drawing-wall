class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, # :registerable,
         :recoverable, :rememberable, :validatable

  has_many :projects, foreign_key: 'created_by'
  has_many :project_invites
  has_many :admin_projects

  def invites
    project_invites
  end

  def project_collaborations
    admin_projects
  end

  def projects_and_collaborations
    Project.where(created_by: self).or(
      Project.where(id: project_collaborations.pluck(:project_id))
    )
  end

  def can_view(record)
    if (record.is_a? Project)
      record.created_by == self || project_collaborations.find_by(project_id: record.id)
    else
      # but now the admin who created the invite can't see it?
      record.admin_id = id
    end
  end

  private

  def set_config_to_default
    update(config: Rails.application.config_for(:drawings).to_json)
  end
end
