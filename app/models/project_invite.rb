class ProjectInvite < ApplicationRecord
  include ActiveModel::Validations

  belongs_to :project
  belongs_to :admin

  validates_with CantBeInvitedToYourOwnProject
  validate :cant_be_invited_if_already_collaborating
  validate :can_only_be_invited_once

  def accept
    AdminProject.create(admin: admin, project: project)
    delete
  end

  def reject
    # we should maybe have a column to denote when an invite was rejected
    delete
  end


  def cant_be_invited_if_already_collaborating
    if (AdminProject.find_by(admin: admin, project: project))
      errors.add(:admin, "#{admin.email} is already a collaborator on this project") 
    end
  end

  def can_only_be_invited_once
    if (invite = ProjectInvite.find_by(admin: admin, project: project))
      errors.add(:project, "#{admin.email} already has a pending invite for this project") unless invite == self
    end
  end
end
