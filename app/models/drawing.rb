class Drawing < ApplicationRecord
  mount_base64_uploader :image, ImageUploader
  after_destroy :delete_linked_file

  belongs_to :project

  def self.to_csv
    attributes = %w{id email name country city accepts_terms created_at image}
    CSV.generate(headers: true) do |csv|
      csv << attributes
      all.each do |drawing|
        csv << attributes.map { |attr| drawing.send(attr) }
      end
    end
  end

  def image_url
    credentials = { aws_access_key_id: ENV['s3_key'], aws_secret_access_key: ENV['s3_secret'] }
    client = Fog::AWS::Storage.new(credentials)
    bucket_name = ENV['s3_bucket']
    client.get_object_url(bucket_name, image.path, 2.seconds.from_now.to_i)
  end

  private

  def delete_linked_file
    self.remove_image!
  end
end
