class AdminProject < ApplicationRecord
  include ActiveModel::Validations

  belongs_to :project
  belongs_to :admin
  validates :admin, uniqueness: { scope: :project }
  validates_with CantBeInvitedToYourOwnProject
end

