class CantBeInvitedToYourOwnProject < ActiveModel::Validator
  def validate(record)
    if record.project.created_by == record.admin
      record.errors.add(:admin, "You can't be invited to your own project") 
    end
  end
end
