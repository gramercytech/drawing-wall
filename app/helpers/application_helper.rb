module ApplicationHelper
	def is_gramercy_email(email)
    	email.include?('@gramercytech.com') || email.include?('jpatuto@gmail.com')
 	end
end
