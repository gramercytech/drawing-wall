source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# auth
gem 'devise'

# environment
gem 'rails', '~> 5.1.6'
gem 'puma', '~> 3.7'
gem 'figaro'
gem 'wdm', '>= 0.1.0' if Gem.win_platform?

# db
gem 'sqlite3', git: "https://github.com/larskanis/sqlite3-ruby", branch: "add-gemspec"
gem 'pg'

# front end
gem 'sass-rails', '~> 5.0'
gem 'milligram-css-rails'
gem 'webpacker'
gem 'react-rails'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'

# attachments
gem 'carrierwave', '~> 1.0'
gem 'carrierwave-base64'
gem 'carrierwave-postgresql'
gem 'fog-aws'
gem 'aws-sdk', '~> 3'
gem 'zipline'

# misc
gem 'jbuilder', '~> 2.5'
gem 'will_paginate', '~> 3.1.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  # gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # gem "capistrano", "~> 3.10", require: false
  gem "capistrano-rails", "~> 1.4", require: false

  gem 'capistrano-rvm'
  gem 'capistrano-bundler', '~> 1.3'
  gem 'capistrano3-nginx', '~> 2.0'
  gem 'capistrano3-puma'
  gem 'capistrano'
  gem 'capistrano-figaro-yml', '~> 1.0.2'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
