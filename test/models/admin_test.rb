require 'test_helper'

class AdminTest < ActiveSupport::TestCase
  test "has invites" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    project_invite = ProjectInvite.create(admin: hank, project: cool_project)
    assert hank.invites.where(project: cool_project).exists?
  end

  test "has collaborations" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    project_invite = ProjectInvite.create(admin: hank, project: cool_project)
    project_invite.accept
    assert hank.project_collaborations.where(project: cool_project).exists?
  end
end
