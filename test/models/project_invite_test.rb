require 'test_helper'

class ProjectInviteTest < ActiveSupport::TestCase
  test "valid project invite" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    invite = ProjectInvite.create(admin: hank, project: cool_project)
    assert invite.valid?
  end

  test "accepting deletes the invite and creates a collaboration" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    invite = ProjectInvite.create(admin: hank, project: cool_project)
    invite.accept
    assert_nil ProjectInvite.find_by(id: invite.id)
    assert hank.project_collaborations.where(project: cool_project).exists?
  end

  test "gets deleted when rejected" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    invite = ProjectInvite.create(admin: hank, project: cool_project)
    invite.reject
    assert_nil ProjectInvite.find_by(id: invite.id)
    assert_not hank.project_collaborations.where(project: cool_project).exists?
  end

  test "can only be invited once" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    ProjectInvite.create(admin: hank, project: cool_project)
    invite = ProjectInvite.create(admin: hank, project: cool_project)
    assert invite.errors.added?(:project, "hank@stricklandpropane.com already has a pending invite for this project")
  end

  test "can not be invited to a project you own" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    invite = ProjectInvite.create(admin: bobby, project: cool_project)
    assert invite.errors.added?(:admin, "You can't be invited to your own project")
  end
  
  test "can not be invited to a project you already collaborate on" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    invite = ProjectInvite.create(admin: hank, project: cool_project)
    invite.accept
    new_invite = ProjectInvite.create(admin: hank, project: cool_project)
    assert new_invite.errors.added?(:admin, "hank@stricklandpropane.com is already a collaborator on this project")
  end

end
