require 'test_helper'

class AdminProjectTest < ActiveSupport::TestCase
  test "valid admin project" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    admin_project = AdminProject.create(admin: hank, project: cool_project)
    assert admin_project.valid?
  end

  test "can not have same admin_id and project_id as another admin_project" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    hank = admins(:hank)
    AdminProject.create(admin: hank, project: cool_project)
    admin_project = AdminProject.create(admin: hank, project: cool_project)
    assert admin_project.errors.added?(:admin, "has already been taken")
  end

  test "can not have an admin_project for a project you created" do
    bobby = admins(:bobby)
    cool_project = Project.create(name: 'Cool Project', created_by: bobby)
    admin_project = AdminProject.create(admin: bobby, project: cool_project)
    assert admin_project.errors.added?(:admin, "You can't be invited to your own project")
  end
end
