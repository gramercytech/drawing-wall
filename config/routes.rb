Rails.application.routes.draw do
  devise_for :admins
  mount ActionCable.server => '/cable'

  root to: 'projects#index'

  resources :projects, only: [:index, :edit, :show]  do
    put '/drawings/live_update', to: 'drawings#live_update'
    resources :drawings, only: [:new, :show, :create, :update]
    resources :exports, only: [:create]
  end

  resources :admins, only: [:index, :edit, :show]

  namespace :api do
    resources :projects, except: [:new, :edit] do
      resources :invites, except: [:show, :new, :edit], controller: 'project_invites'
      resources :collaborators, only: [:index, :destroy]
      resources :drawings, only: [:index, :update]
    end

    resources :admins, except: [:new, :edit]

    resources :admin_invites, only: [:index, :update, :destroy]
  end
end
