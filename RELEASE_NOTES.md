**August 2023**

[X] Add background

[X] Convert some of the Tablet app to modern React
  - Trying to get as much of this project as possible moved onto functional React with hooks
  - Also trying to move away from importing too much CSS for UI-heavy components.  A lot of styling for the Art screen was rewritten using the styled-components package.
  - Given we have a robust UI framework, some manually created event listeners were replace or removed
  - A bunch of the project has been reformatted with Prettier.

**December 2021 Release Notes**

[X] Deploy to remote server
  - `$ cap production deploy BRANCH=2021-12-24__deploy-to-remote-server` (I will eventually merge this work into master)
  - EC2 -> nginx -> puma

[X] Data is stored in the cloud
  - RDS (postgres)

[X] Drawings are stored in the cloud
  - S3

[X] Authorization has been added to prevent foul play from malicious users.
  - Use the rails console to create new admins (`Admin.create(email: 'example@email.com', password: 'awefawef', password_confirmation: 'awefawef')`) -- admins cannot currently be created through the UI.

[X] Project Flow 
  - Admins can create projects
  - Drawings are scoped to projects, as well as the socket channels used to broadcast live updates
  - Admins can invite other admins as collaborators

[X] Configuring the visual elements of the wall can now be done against a project record
  - YAML configs can still be leveraged like so: `Project.first.update(config: Rails.application.config_for(:drawings))` where `:drawings` can be replaced with any .yaml file in config/ directory.
  - Some styles have been moved from scss into react using styled-components to accommodate this update.

 
*Notes about versions*

  - ruby 2.7.2
  - node 12.22.8 (this one is particular - later versions of node often don't play nicely with node-sass library, which is a dependency either in webpacker or react, I'm not sure, but I have run into this many times)
  - yarn 1.22.17 (what matters here is not the version but how it is installed, please use `npm install -g yarn` -- do *not* install via brew or apt

