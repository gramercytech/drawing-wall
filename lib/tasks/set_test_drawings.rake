namespace :set_test_drawings do
  task go: :environment do
    Drawing.update_all(test: true)
  end
end
